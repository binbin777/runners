// 获取【数据源】的统一接口
export const selectList = '/publicData/getAllMess';

// 获取【结账检查】列表接口
export const selectCheckoutList = '/monthSettle/recentSettleDate';
export const selectCheck = '/monthSettle/settleDateCheck';

// 【前置处理】
export const selectTable = '/processPre/list';// 前置列表表格数据
export const selectRate = '/exrateMonth/exrate/RMB2dollar';// 前置1
export const selectCalculate = '/revenueCostSummary/summary';// 前置2

// 【销售收入&成本统计】
export const selectListAccountPage = '/revenueCostSummary/list';

// 【营销费导入统计】
export const selectImportAccountPage = '/marketerFee/summary/list';

// 【营销费导入】确认导入接口
export const confirmExport = '/marketerFee/importMarketerFeeFile';

// 获取【印度&中东分摊营销费】列表接口
export const selectMarketFeeShareTable = '/marketerFee/indiaAndMiddleEast/list';

// 获取【emc&ali分摊营销费】列表接口
export const selectemcaliAccountPage = '/marketerFee/emcAndAli/list';

// 获取【毛利分析】列表接口
export const selectMaoriTable = '/commodityGrossProfit/analysis/list';
// 导出
export const exportData = '/commodityGrossProfit/analysis/export';

// 获取【手续费统计】列表接口
export const selectFeeSummary = '/cleaningFeeSummary/list';

// 获取【仓储费&物流费用统计】列表接口
export const selectWarehouseLogisticsSummary = '/warehouseLogisticsSummary/list';

// 获取【待摊分摊表】列表接口
export const selectExpense = '/share/expense/fee/list';

// 获取【站点利润分析】列表接口
export const selectSiteProfit = '/siteProfit/analysis/list';
export const exportDataSite = '/siteProfit/analysis/export';

// 获取【国家利润分析】列表接口
export const selectCountryProfit = '/countryProfit/analysis/list';
export const exportDataCountry = '/countryProfit/analysis/export';

// 获取【国家利润分析】列表接口
export const selectCountryCostFee = '/countryCostFee/analysis/list';
export const exportDataCountryCostFee = '/countryCostFee/analysis/export';

// 获取【国家地区配置】列表接口
export const selectCountryArea = '/marketerFee/countryArea/list';
export const addtCountryArea = '/marketerFee/countryArea/add';
// export const exportDataCountryCostFee = '/countryCostFee/analysis/export';

// CODS
export const selectCodsServiceFee = '/codsServiceFee/summary';
export const deleteCODS = '/codsServiceFee/delete';
export const confirmCODS = '/codsServiceFee/importFile';

// FBA
export const selectFBAFreight = '/fbaFreight/summary';
export const deleteFBA = '/fbaFreight/delete';
export const confirmFBA = '/fbaFreight/importFile';

// 仓储费
export const selectWarehousingFee = '/warehousingFee/summary';
export const deleteWare = '/warehousingFee/delete';
export const confirmWare = '/warehousingFee/importFile';

