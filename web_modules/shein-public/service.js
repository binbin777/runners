import { toQueryString, toQueryString1 } from 'shein-lib/query-string';
import request from 'shein-lib/fetch';
import * as api from './api-address';

/* ---------------------------------------公共接口-------------------------------- */
export const selectList = () => (
  request(api.selectList, { method: 'GET' })
); // 0. 获取下拉框数据源

/* --- 1.结账检查 --- */
export const selectCheckoutList = (filter) => {
  const keys = ['companyId'];
  return request(`${api.selectCheckoutList}?${toQueryString1(keys, filter)}`, { method: 'GET' });
}; // 搜索-获取列表
export const selectCheck = () => request(`${api.selectCheck}`, { method: 'GET' }); // 搜索-结账检查

/* --- 2.前置处理 --- */
export const selectTable = (filter) => {
  const keys = ['financialPeriod'];
  return request(`${api.selectTable}?${toQueryString1(keys, filter)}`, { method: 'GET' });
};
export const selectRate = (filter) => {
  const keys = ['financialPeriod'];
  return request(`${api.selectRate}?${toQueryString1(keys, filter)}`, { method: 'GET' });
}; // 前置1 获取汇率

export const selectCalculate = (filter, d) =>
  request(`${d}`, { method: 'POST', body: filter }); // JSON.stringify(filter)
// 前置2 计算,同步

export const selectFresh = (filter, d) => {
  const keys = ['processPreId'];
  return request(`${d}?${toQueryString1(keys, filter)}`, { method: 'GET' });
}; // 刷新
// export const selectSame = (filter, d) =>
//   request(`${d}`, { method: 'POST', body: filter }); // JSON.stringify(filter)
// // 前置2 同步

/* --- 3.前置处理-销售收入&成本统计 --- */
export const selectListAccountPage = (filter) => {
  const keys = ['financialPeriod', 'companyId', 'siteId', 'countryId', 'lastCategoryId', 'pageNumber', 'pageSize'];
  return request(`${api.selectListAccountPage}?${toQueryString1(keys, filter)}`, { method: 'GET' });
};

/* --- 4.前置处理-营销费导入统计 --- */
export const selectImportAccountPage = (filter) => {
  const keys = ['financialPeriod', 'siteId', 'pageNumber', 'pageSize'];
  return request(`${api.selectImportAccountPage}?${toQueryString1(keys, filter)}`, { method: 'GET' });
};

/* --- 5.前置处理-营销费导入 --- */
export const confirm = (filter) => {
  const keys = ['financialPeriod', 'processPreId'];
  return request(`${api.confirmExport}?${toQueryString1(keys, filter)}`, { method: 'GET' });
};// 确认导入
/* --- 6.前置处理-（印度&中东）营销费分摊表 ---*/
export const selectMarketFeeShareTable = (filter) => {
  const keys = ['siteId', 'countryId', 'financialPeriod'];
  return request(`${api.selectMarketFeeShareTable}?${toQueryString(keys, filter)}`, { method: 'GET' });
}; // 搜索-获取列表

/* --- 7.前置处理-（emc&ali）营销费分摊表 --- */
export const selectemcaliAccountPage = (filter) => {
  const keys = ['siteId', 'financialPeriod'];
  return request(`${api.selectemcaliAccountPage}?${toQueryString1(keys, filter)}`, { method: 'GET' });
}; // 搜索-获取列表

/* --- 8.商品毛利分析 --- */
export const selectMaoriTable = (filter) => {
  const keys = ['financialPeriod', 'categoryType', 'lastCategory', 'categoryLevel', 'pageNumber', 'pageSize'];
  return request(`${api.selectMaoriTable}?${toQueryString(keys, filter)}`, { method: 'GET' });
}; // 搜索-获取列表
const selectOutinPageKeys = ['financialPeriod', 'categoryType', 'lastCategory', 'categoryLevel'];
export const exportData = filter =>
  (request(`${api.exportData}?${toQueryString(selectOutinPageKeys.filter(item => (item !== 'pageNumber' && item !== 'pageSize')), filter)}`, { method: 'GET' })); // 导出列表

// 9. 手续费统计
export const selectFeeSummary = (filter) => {
  const keys = ['financialPeriod', 'companyId', 'siteId', 'countryId', 'orderType', 'pageNumber', 'pageSize'];
  return request(`${api.selectFeeSummary}?${toQueryString1(keys, filter)}`, { method: 'GET' });
};

// 10. 仓储费&物流费用统计
export const selectWarehouseLogisticsSummary = (filter) => {
  const keys = ['financialPeriod', 'companyId', 'siteId', 'countryId', 'orderType', 'supplierId', 'projectType', 'pageNumber', 'pageSize'];
  return request(`${api.selectWarehouseLogisticsSummary}?${toQueryString1(keys, filter)}`, { method: 'GET' });
};

// 18. 待摊分摊表
export const selectExpense = (filter) => {
  const keys = ['financialPeriod', 'feeType', 'orderType', 'supplierId', 'pageNumber', 'pageSize'];
  return request(`${api.selectExpense}?${toQueryString1(keys, filter)}`, { method: 'GET' });
};

// 19 站点利润分析
export const selectSiteProfit = (filter) => {
  const keys = ['financialPeriod', 'siteId', 'countryId', 'pageNumber', 'pageSize'];
  return request(`${api.selectSiteProfit}?${toQueryString1(keys, filter)}`, { method: 'GET' });
};
const selectOutinPageKeys2 = ['financialPeriod', 'siteId', 'countryId'];
export const exportDataSite = filter =>
  (request(`${api.exportDataSite}?${toQueryString(selectOutinPageKeys2.filter(item => (item !== 'pageNumber' && item !== 'pageSize')), filter)}`, { method: 'GET' })); // 导出列表

// 20 国家利润分析
export const selectCountryProfit = (filter) => {
  const keys = ['financialPeriod', 'siteId', 'countryId', 'pageNumber', 'pageSize'];
  return request(`${api.selectCountryProfit}?${toQueryString1(keys, filter)}`, { method: 'GET' });
};
export const exportDataCountry = filter =>
  (request(`${api.exportDataCountry}?${toQueryString(selectOutinPageKeys2.filter(item => (item !== 'pageNumber' && item !== 'pageSize')), filter)}`, { method: 'GET' })); // 导出列表

// 20 国家成本费用分析
export const selectCountryCostFee = (filter) => {
  const keys = ['financialPeriod', 'countryId'];
  return request(`${api.selectCountryCostFee}?${toQueryString1(keys, filter)}`, { method: 'GET' });
};
const selectOutinPageKeys3 = ['financialPeriod', 'countryId'];
export const exportDataCountryCostFee = filter =>
  (request(`${api.exportDataCountryCostFee}?${toQueryString(selectOutinPageKeys3.filter(item => (item !== 'pageNumber' && item !== 'pageSize')), filter)}`, { method: 'GET' })); // 导出列表

// 11 国家地区配置
export const selectCountryArea = (filter) => {
  const keys = ['siteId', 'countryId'];
  return request(`${api.selectCountryArea}?${toQueryString1(keys, filter)}`, { method: 'GET' });
};
export const countryDelete = id =>
  request(`/marketerFee/countryArea/${id}/delete`, { method: 'POST', body: { id } });

export const addtCountryArea = filter =>
  request(`${api.addtCountryArea}`, { method: 'POST', body: filter });

// 12 CODS服务费统计
export const selectCodsServiceFee = (filter) => {
  const keys = ['financialPeriod', 'pageNumber', 'pageSize'];
  return request(`${api.selectCodsServiceFee}?${toQueryString1(keys, filter)}`, { method: 'GET' });
};
export const CODSDelete = id =>
  request(`${api.deleteCODS}`, { method: 'POST', body: { id } });

// 13 cods导入
export const confirmCODS = (filter) => {
  const keys = ['financialPeriod', 'processPreId'];
  return request(`${api.confirmCODS}?${toQueryString1(keys, filter)}`, { method: 'GET' });
}; // 确认导入

// 14 FBA运费统计
export const selectFBAFreight = (filter) => {
  const keys = ['financialPeriod', 'pageNumber', 'pageSize', 'supplierId'];
  return request(`${api.selectFBAFreight}?${toQueryString1(keys, filter)}`, { method: 'GET' });
};
export const FBADelete = id =>
  request(`${api.deleteFBA}`, { method: 'POST', body: { id } });

// 15 FBA导入
export const confirmFBA = (filter) => {
  const keys = ['financialPeriod', 'processPreId'];
  return request(`${api.confirmFBA}?${toQueryString1(keys, filter)}`, { method: 'GET' });
}; // 确认导入

// 16 仓储费统计
export const selectWarehousingFee = (filter) => {
  const keys = ['financialPeriod', 'pageNumber', 'pageSize', 'countryId', 'siteId'];
  return request(`${api.selectWarehousingFee}?${toQueryString1(keys, filter)}`, { method: 'GET' });
};
export const deleteWare = id =>
  request(`${api.deleteWare}`, { method: 'POST', body: { id } });

// 17 仓储费导入
export const confirmWare = (filter) => {
  const keys = ['financialPeriod', 'processPreId'];
  return request(`${api.confirmWare}?${toQueryString1(keys, filter)}`, { method: 'GET' });
}; // 确认导入
