import React from 'react';

export default () => (
  <span>数据导出文件生成成功。<br />请在主数据系统-导出下载里下载文件</span>
);
