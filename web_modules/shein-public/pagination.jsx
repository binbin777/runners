import React from 'react';
import PropTypes from 'prop-types';
import { Pagination } from 'antd';

const Page = (props) => {
  const { onChange, total, current } = props;
  return (
    <Pagination
      style={{ float: 'right', margin: '25px 10px' }}
      showQuickJumper
      showSizeChanger
      defaultCurrent={1}
      defaultPageSize={20}
      current={current || 1}
      total={total || 0}
      pageSizeOptions={['20', '50', '100']}
      showTotal={count => `共 ${count} 条记录`}
      onChange={(page, size) => onChange(page, size)}
      onShowSizeChange={(page, size) => onChange(page, size)}
    />
  );
};
Page.propTypes = {
  onChange: PropTypes.func.isRequired,
  total: PropTypes.number.isRequired,
  current: PropTypes.number.isRequired,
};
export default Page;

