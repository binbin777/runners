import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { DatePicker } from 'antd';
import moment from 'moment';

class Wrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      start: this.props.value[0],
      end: this.props.value[1],
      disabledDuration: this.props.disabledDuration,
      format: this.props.format,
    };
  }
  componentWillReceiveProps(np) {
    const {
      value, disabledDuration, format,
    } = this.props;
    if (value[0] !== np.value[0] || value[1] !== np.value[1] || disabledDuration !== np.disabledDuration || format !== np.format) {
      this.setState({
        start: np.value[0],
        end: np.value[1],
        disabledDuration: np.disabledDuration,
        format: np.format,
      });
    }
  }
  render() {
    const {
      start, end, disabledDuration, format,
    } = this.state;
    return (
      <Fragment>
        <DatePicker
          format={format}
          value={start ? moment(start, format) : null}
          onChange={(_, value) => {
            this.setState({ start: value });
            this.props.onChange([value, end]);
          }}
          disabledDate={(startValue) => {
            if (!startValue || !end) {
              return false;
            }
            return startValue.valueOf() >= moment(end, format).valueOf() || (disabledDuration && startValue.valueOf() <= moment(end, format).subtract(disabledDuration + 1, 'day').valueOf());
          }}
        />
        <span style={{ margin: '0 10px' }}>至</span>
        <DatePicker
          format={format}
          value={end ? moment(end, format) : null}
          onChange={(_, value) => {
            this.setState({ end: value });
            this.props.onChange([start, value]);
          }}
          disabledDate={(endValue) => {
            if (!endValue || !start) {
              return false;
            }
            return endValue.valueOf() <= moment(start, format).valueOf() || (disabledDuration && endValue.valueOf() >= moment(start, format).add(disabledDuration + 1, 'day').valueOf());
          }}
        />
      </Fragment>
    );
  }
}


Wrapper.propTypes = {
  value: PropTypes.arrayOf(PropTypes.string),
  disabledDuration: PropTypes.number,
  format: PropTypes.string,
  onChange: PropTypes.func,
};
export default Wrapper;
