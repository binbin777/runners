/* eslint-disable */
const webpack = require('webpack');
const path = require('path');
const config = require('./webpack.base.config.js');
const apiPathUri = {
  dev: 'http://192.168.1.159:13002',
  test: 'http://test.fmis.lt'
};

module.exports = Object.assign({},config, {
  output: {
    path: path.resolve(__dirname, 'bundle'),
    filename: '[name].js',
    chunkFilename: '[name].js',
    publicPath: '/'
  },
  module: {
    rules: [
      ...config.module.rules,
      {
        test: /\.css$/,
        loaders: ['happypack/loader?id=styles']
      }
    ]
  },
  plugins: [
    ...config.plugins,
    new webpack.DefinePlugin({
      process: {
        env: {
          LOGIN: JSON.stringify(process.env.LOGIN || 'http://auth.shein.com'),
          NODE_ENV: JSON.stringify('development'),
          BASE_URI: JSON.stringify(process.env.BASE_URI || '/fas'),
        }
      }
    }),
  ],
  devServer: {
    host: '0.0.0.0',
    contentBase: [path.join(__dirname, './')],
    port: 8081,
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    proxy: {
      '/fis': {
        target: 'http://fas-dev01.dotfashion.cn',
        // pathRewrite: { '/api': '' },
        secure: false,
        changeOrigin: true
      },
      // '/fas': {
      //   target: 'http://192.168.1.159:13806/fas',
      //   pathRewrite: { '^/fas': '' },
      //   secure: false,
      //   changeOrigin: true
      // },
      '/fas': {
        target: 'http://fas-dev01.dotfashion.cn',
       // pathRewrite: { '^/fas': '' },
        secure: false,
        changeOrigin: true
      },
      'local':{
        target: 'http://192.168.20.245:9091/fas',
         pathRewrite: { '^/local': '' },
        secure: false,
        changeOrigin: true
      },
    },
    open: false
  }
});
