/**
 * Created by fed on 2017/4/11.
 */
exports.externals = {
  react: 'window.React',
  'react-dom': 'window.ReactDOM',
  antd: 'window.antd',
  moment: 'window.moment',
  lodash: 'window._',
  'babel-polyfill': 'undefined',
  'react-router-dom': 'window.ReactRouterDOM',
  history: 'window.History',
  redux: 'window.Redux',
  'redux-saga': 'window.ReduxSaga',

};

const combinePkg = [
  ['babel-polyfill', ['polyfill.min.js']],
  ['react', ['/umd/react.production.min.js', '/umd/react.development.js']],
  ['react-dom', ['/umd/react-dom.production.min.js', '/umd/react-dom.development.js']],
  ['react-router-dom', ['react-router-dom.min.js']],
  ['history', ['history.min.js']],
  ['redux', ['redux.min.js']],
  ['redux-saga', ['redux-saga.min.js']],
];
const singlePkg = [
  ['lodash', ['lodash.min.js']],
  ['moment', ['moment-with-locales.min.js']],
  ['antd', ['antd-with-locales.min.js', 'antd-with-locales.js']],
];


exports.forCombineCDNScript = combinePkg.map(([name, pa]) => [
  name,
  pa,
  require('./package-lock.json').dependencies[name.toLowerCase()],
]);

exports.forSingleCDNScript = singlePkg.map(([name, pa]) => [
  name,
  pa,
  require('./package-lock.json').dependencies[name.toLowerCase()],
]);
