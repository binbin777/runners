import 'classnames';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import hashHistory from 'shein-lib/history';
import { LocaleProvider } from 'antd';
import zhCN from 'antd/lib/locale-provider/zh_CN';
import { conf } from 'sheinq';

import store from './createStore';

import RootView from './component/root';


// 设置缓存发送埋点
conf({
  needCache: {
    request: 10, event: 10, pv: 10, leave: 10,
  },
  request: { threshold: 100 }, // 慢接口阈值
});

if (process.env.AUTH_ENV)conf({ authEnv: process.env.AUTH_ENV });

const root = (
  <Provider store={store}>
    <LocaleProvider locale={zhCN}>
      <RootView history={hashHistory} innerStore={store} />
    </LocaleProvider>
  </Provider>
);

render(
  root,
  document.getElementById('container'),
);
