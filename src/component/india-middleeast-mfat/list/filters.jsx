import React from 'react';
import PropTypes from 'prop-types';
import assign from 'object-assign';
import { Form, Select, DatePicker } from 'shineout';
import moment from 'moment';
import { changeFilter, search } from './action';
import Style from './style.css';


const Filters = (props) => {
  const {
    dispatch, filter, siteList, countryList, params,
  } = props;
  return (
    <div className={Style.content}>
      <Form
        inline
        onSubmit={(values) => {
            dispatch(search(assign({}, values, { pageNumber: 1, pageSize: filter.pageSize })));
          }}
      >
        <Form.Item>
          <Select
            name="siteId"
            keygen="id"
            style={{ width: 160 }}
            datum={{ format: 'id' }}
            renderItem={item => item.name}
            data={siteList}
            placeholder="站点"
            clearable
            onFilter={text => item => item.name.toUpperCase().indexOf(text.toUpperCase()) >= 0}
            onChange={(d) => {
              dispatch(changeFilter('siteId', d));
            }}
          />
        </Form.Item>
        <Form.Item>
          <Select
            name="countryId"
            keygen="id"
            style={{ width: 160 }}
            onChange={(d) => {
                dispatch(changeFilter('countryId', d));
              }}
            datum={{ format: 'id' }}
            renderItem={item => item.nameCn}
            data={countryList}
            placeholder="国家"
            clearable
            onFilter={text => item => item.nameCn.indexOf(text) >= 0}
          />
        </Form.Item>
        <Form.Item>
          <DatePicker
            clearable={false}
            name="financialPeriod"
            style={{ width: 160 }}
            type="month"
            // defaultValue={moment().subtract(1, 'months').format('YYYY-MM')}
            defaultValue={moment(params.id ? params.id : '').format('YYYY-MM')}
            onChange={(d) => {
              dispatch(changeFilter('financialPeriod', d));
            }}
          />
        </Form.Item>
        <Form.Item>
          <Form.Submit>
              查询
          </Form.Submit>
        </Form.Item>
      </Form>
    </div>
  );
};

Filters.propTypes = {
  dispatch: PropTypes.func.isRequired,
  filter: PropTypes.shape({}).isRequired,
  siteList: PropTypes.arrayOf(PropTypes.object).isRequired,
  countryList: PropTypes.arrayOf(PropTypes.object).isRequired,
  params: PropTypes.shape().isRequired,
};
export default Filters;
