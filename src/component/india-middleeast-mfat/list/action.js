import * as types from './types';

export const init = () => ({ type: types.init });
export const changeFilter = (key, value) => ({ type: types.changeFilter, key, value });
export const changeValue = (key, value) => ({ type: types.changeValue, key, value });
export const getList = data => ({ type: types.getList, data });
export const getListSuc = data => ({ type: types.getListSuc, data });
export const search = filter => ({ type: types.search, filter });
export const searchSuc = data => ({ type: types.searchSuc, data });

