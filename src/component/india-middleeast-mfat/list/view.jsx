/*
*  前置处理 -（印度&中东）营销费分摊表 by jinchao
*/
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Filters from './filters';
import TableView from './table';
import { init, getList, changeFilter } from './action';

class list extends Component {
  constructor(props) {
    super(props);
    const { dispatch, params } = props;
    dispatch(init());
    dispatch(getList());
    dispatch(changeFilter('financialPeriod', params.id ? params.id : ''));
  }
  render() {
    return (
      <div>
        <Filters {...this.props} />
        <TableView {...this.props} />
      </div>
    );
  }
}

list.propTypes = {
  dispatch: PropTypes.func,
  params: PropTypes.shape().isRequired,
};
const mapStateToProps = state => state['india-middleeast-mfat/list'];
export default connect(mapStateToProps)(list);
