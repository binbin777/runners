import * as types from './types';

export const defaultState = {
  filter: {
    siteId: '',
    countryId: '',
    financialPeriod: '',
  },
  load: false,
  siteList: [],
  countryList: [],
  dataSource: [],
  total: 0,
};

const reducer = {
  defaultState,
  [types.init]: () => defaultState,
  [types.changeValue]: (state, { key, value }) => { state[key] = value; },
  [types.changeFilter]: (state, { key, value }) => { state.filter[key] = value; },
  [types.getListSuc]: (state, { data }) => {
    state.siteList = data.siteList;
    state.countryList = data.countryList;
  },
  [types.search]: (state, { filter }) => {
    state.load = true;
    state.filter = filter;
  },
  [types.searchSuc]: (state, { data }) => {
    state.dataSource = data.data;
    state.load = false;
  },
};
export default reducer;
