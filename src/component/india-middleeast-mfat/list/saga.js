
import { message } from 'antd';
import { takeLatest, takeEvery, put } from 'redux-saga/effects';
import { selectList, selectMarketFeeShareTable } from 'shein-public/service';

import * as types from './types';
import { getListSuc, changeValue, searchSuc } from './action';

// 获取数据源列表
// function* getList(action) {
//   const data = yield selectList(action.data);
//   if (data.msg) {
//     return message.error(`获取数据失败: ${data.msg}`);
//   }
//   return yield put(getListSuc(data.data));
// }
function* getList() {
  const data = yield selectList();
  if (data.code !== '0') {
    return message.error(`获取数据失败: ${data.msg}`);
  }
  return yield put(getListSuc(data.data));
}

// 查询&重新计算
function* search(action) {
  const data = yield selectMarketFeeShareTable(action.filter);
  if (data.msg) {
    message.error(`获取数据失败: ${data.msg}`);
    return yield put(changeValue('load', false));
  }
  return yield put(searchSuc(data));
}

export default function* () {
  yield takeLatest(types.search, search);
  yield takeEvery(types.getList, getList);
}
