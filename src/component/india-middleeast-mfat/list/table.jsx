import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import ThousandsPoints from '../../publicComponent/thousandsPoints';

const TableView = ({
  dataSource,
  load,
}) => (
  <Table
    rowKey="id"
    pagination={false}
    dataSource={dataSource}
    loading={load}
    size="small"
    scroll={{ x: 1600 }}
    columns={[
      {
        title: '站点',
        dataIndex: 'siteVal',
        render: text => text || '--',
      },
      {
        title: '国家',
        dataIndex: 'countryVal',
        render: text => text || '--',
      },
      {
        title: '期间',
        dataIndex: 'financialPeriod',
        width: 90,
        render: text => text || '--',
      },
      {
        title: '营销费$',
        align: 'right',
        dataIndex: 'marketerFee',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '在线支付-收入$',
        align: 'right',
        dataIndex: 'recognizeRevenueOnline',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: 'COD支付-收入$',
        align: 'right',
        dataIndex: 'codRecognizeRevenue',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '在线支付-收入占比%',
        dataIndex: 'recognizeRevenueOnlineRatio',
        render: text => text || '--',
      },
      {
        title: 'COD支付-收入占比%',
        dataIndex: 'codRecognizeRevenueRatio',
        render: text => text || '--',
      },
      {
        title: '在线支付—分摊营销费用$',
        align: 'right',
        dataIndex: 'onlineMarketerFee',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: 'COD支付—分摊营销费用$',
        align: 'right',
        dataIndex: 'codMarketerFee',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '更新时间',
        dataIndex: 'updateTime',
        width: 200,
        render: text => text || '--',
      },
    ]}
  />
);

TableView.propTypes = {
  dataSource: PropTypes.arrayOf(PropTypes.object).isRequired,
  load: PropTypes.bool,
};

export default TableView;
