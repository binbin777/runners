import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import ThousandsPoints from '../../publicComponent/thousandsPoints';

const TableView = ({
  dataSource,
  load,
}) => (
  <Table
    rowKey="id"
    pagination={false}
    dataSource={dataSource}
    loading={load}
    size="small"
    scroll={{ x: 1700 }}
    columns={[
      {
        title: '站点',
        dataIndex: 'siteVal',
        render: text => text || '--',
      },
      {
        title: '国家',
        dataIndex: 'countryVal',
        render: text => text || '--',
      },
      {
        title: '财务期间',
        dataIndex: 'financialPeriod',
        render: text => text || '--',
      },
      {
        title: '收入$',
        dataIndex: 'revenue',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '销售成本$',
        dataIndex: 'cost',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '营销费$',
        dataIndex: 'marketerFee',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '手续费$',
        dataIndex: 'cleaningFee',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '运费$',
        dataIndex: 'freight',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '仓储费$',
        dataIndex: 'warehousingFee',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '税费$',
        dataIndex: 'taxation',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: 'CODS服务费$',
        dataIndex: 'codsServiceFee',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '利润$',
        dataIndex: 'profit',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '销售成本占比%',
        dataIndex: 'costRatio',
        render: (text, record) => (record.revenue === 0.00 ? '#DIV/0!' : text || '--'),
      },
      {
        title: '营销费占比%',
        dataIndex: 'marketerFeeRatio',
        render: (text, record) => (record.revenue === 0.00 ? '#DIV/0!' : text || '--'),
      },
      {
        title: '手续费占比%',
        dataIndex: 'cleaningFeeRatio',
        render: (text, record) => (record.revenue === 0.00 ? '#DIV/0!' : text || '--'),
      },
      {
        title: '运费占比%',
        dataIndex: 'freightRatio',
        render: (text, record) => (record.revenue === 0.00 ? '#DIV/0!' : text || '--'),
      },
      {
        title: '仓储费占比%',
        dataIndex: 'warehousingFeeRatio',
        render: (text, record) => (record.revenue === 0.00 ? '#DIV/0!' : text || '--'),
      },
      {
        title: '税费占比%',
        dataIndex: 'taxationRatio',
        render: (text, record) => (record.revenue === 0.00 ? '#DIV/0!' : text || '--'),
      },
      {
        title: 'CODS服务费占比%',
        dataIndex: 'codsServiceFeeRatio',
        render: (text, record) => (record.revenue === 0.00 ? '#DIV/0!' : text || '--'),
      },
      {
        title: '利润占比%',
        dataIndex: 'profitRatio',
        render: (text, record) => (record.revenue === 0.00 ? '#DIV/0!' : text || '--'),
      },
    ]}
  />
);

TableView.propTypes = {
  dataSource: PropTypes.arrayOf(PropTypes.object),
  load: PropTypes.bool,
};

export default TableView;
