import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Select, DatePicker, Button, Datum } from 'shineout';
import moment from 'moment';
import { changeFilter, search, exportData } from './action';
import Style from './style.css';

class Filters extends Component {
  constructor(props) {
    super(props);
    this.datum = new Datum.Form();
    this.handleExport = this.handleExport.bind(this);
  }
  handleExport() {
    this.datum.validate().then((res) => {
      if (res === true) this.props.dispatch(exportData(this.datum.$values));
    }).catch((e) => {
      console.log(e);
    });
  }
  render() {
    const {
      dispatch,
      filter,
      siteList,
      countryList,
    } = this.props;
    return (
      <div className={Style.content}>
        <Form
          inline
          datum={this.datum}
          onSubmit={(values) => {
            dispatch(search(Object.assign({}, values, { pageNumber: 1, pageSize: filter.pageSize })));
          }}
        >
          <Form.Item
            className={Style.filterItem}
          >
            <Select
              name="siteId"
              keygen="id"
              style={{ width: 140 }}
              data={siteList}
              onChange={d => dispatch(changeFilter('siteId', d))}
              datum={{ format: 'id' }}
              renderItem={item => item.name}
              clearable
              placeholder="站点"
              onFilter={text => item => item.name.toUpperCase().indexOf(text.toUpperCase()) >= 0}
            />
          </Form.Item>
          <Form.Item
            className={Style.filterItem}
          >
            <Select
              name="countryId"
              keygen="id"
              style={{ width: 160 }}
              data={countryList}
              onChange={d => dispatch(changeFilter('countryId', d))}
              datum={{ format: 'id' }}
              renderItem={item => item.nameCn}
              placeholder="国家"
              clearable
              onFilter={text => item => item.nameCn.indexOf(text) >= 0}
            />
          </Form.Item>
          <Form.Item
            className={Style.filterItem}
          >
            <DatePicker
              clearable={false}
              name="financialPeriod"
              type="month"
              // defaultValue={moment().subtract(1, 'months').format('YYYY-MM')}
              defaultValue={moment().subtract(1, 'months').format('YYYY-MM')}
              // value={filter.financialPeriod}
              style={{ width: 140 }}
              onChange={d => dispatch(changeFilter('financialPeriod', d))}
            />
          </Form.Item>
          <Form.Item
            className={Style.filterButton}
          >
            <Form.Submit>查询</Form.Submit>
            <Button
              onClick={this.handleExport}
            >
              导出
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}
Filters.propTypes = {
  dispatch: PropTypes.func.isRequired,
  filter: PropTypes.shape({}).isRequired,
  siteList: PropTypes.arrayOf(PropTypes.shape({})),
  countryList: PropTypes.arrayOf(PropTypes.shape({})),
};
export default Filters;
