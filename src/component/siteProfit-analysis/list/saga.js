
import { message } from 'antd';
import { takeLatest, takeEvery, put } from 'redux-saga/effects';
import ExportModal from 'shein-public/exportModal';
import {
  selectList,
  selectSiteProfit,
  exportDataSite,
} from 'shein-public/service';

import * as types from './types';
import { searchSuc, changeValue, getListSuc } from './action';

function* search(action) {
  const data = yield selectSiteProfit(action.filter);
  if (!data || data.code !== '0') {
    message.error(`获取数据失败: ${data.msg}`);
    return yield put(changeValue('load', false));
  }
  return yield put(searchSuc(data.data));
}

function* getList() {
  const data = yield selectList();
  if (data.code !== '0') {
    return message.error(`获取数据失败: ${data.msg}`);
  }
  return yield put(getListSuc(data.data));
}
function* exportDataSaga(action) {
  const data = yield exportDataSite(action.filter);
  if (data.code !== '0') {
    message.error(`导出数据失败: ${data.msg}`);
    return yield put(changeValue('load', false));
  }
  message.success(<ExportModal />);
  return yield put(changeValue('load', false));
}

export default function* () {
  yield takeLatest(types.search, search);
  yield takeEvery(types.getList, getList);
  yield takeLatest(types.exportData, exportDataSaga);
}
