import moment from 'moment';
import * as types from './types';

export const defaultState = {
  filter: {
    pageSize: 20,
    pageNumber: 1,
    financialPeriod: moment().subtract(1, 'months').format('YYYY-MM'),
    siteId: '',
    countryId: null,
  },
  dataSource: [],
  load: false,
  maintenanceTime: '',
  curRate: null,
  total: 0,
  siteList: [],
  sumInfo: {},
  countryList: [],
};

const reducer = {
  defaultState,
  [types.initData]: () => defaultState,
  [types.changeValue]: (state, { key, value }) => { state[key] = value; },
  [types.changeFilter]: (state, { key, value }) => { state.filter[key] = value; },
  [types.search]: (state, { filter }) => {
    state.load = true;
    state.filter.pageSize = filter.pageSize;
    state.filter.pageNumber = filter.pageNumber;
  },
  [types.searchSuc]: (state, { data }) => {
    state.dataSource = data.rows;
    if (data.sumInfo) { state.dataSource.push(data.sumInfo); }
    state.total = data.total;
    state.maintenanceTime = data.maintenanceTime;
    state.curRate = data.curRate;
    state.load = false;
  },
  [types.getListSuc]: (state, { data }) => {
    state.siteList = data.siteList;
    state.countryList = data.countryList;
  },
};
export default reducer;
