import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';

const TableView = ({
  dataSource,
  load,
}) => (
  <Table
    rowKey="id"
    pagination={false}
    dataSource={dataSource}
    loading={load}
    size="small"
    columns={[
      {
        title: '公司主体',
        dataIndex: 'companyName',
        render: text => text || '--',
      },
      {
        title: '财务子系统',
        dataIndex: 'sourceSystemVal',
        render: text => text || '--',
      },
      {
        title: '最近结账期间',
        dataIndex: 'settleDate',
        render: text => text || '--',
      },
      {
        title: '最近结账时间',
        dataIndex: 'settleTime',
        render: text => text || '--',
      },
    ]}
  />
);

TableView.propTypes = {
  dataSource: PropTypes.arrayOf(PropTypes.object).isRequired,
  load: PropTypes.bool.isRequired,
};

export default TableView;
