import * as types from './types';

export const defaultState = {
  companyId: null,
  load: false,
  loadBtn: false,
  companyList: [],
  dataSource: [],
  recentTime: '--',
};

const reducer = {
  defaultState,
  [types.initData]: () => (defaultState),
  [types.changeValue]: (state, { key, value }) => { state[key] = value; },
  [types.getListSuc]: (state, { data }) => {
    state.companyList = data.companyList;
  },
  [types.search]: (state) => {
    state.load = true;
  },
  [types.check]: (state) => {
    // state.load = true;
    state.loadBtn = true;
  },
  [types.searchSuc]: (state, { data }) => {
    state.dataSource = data.rows;
    state.recentTime = data.lastCheckTime;
    state.load = false;
  },
};
export default reducer;
