/*
*  结账检查 by jinchao
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Filters from './filters';
import TableView from './table';
import { getList, initData } from './action';
import Style from './style.css';

const suggestion = '财务子系统的财务期间结账后，开始财务分析，可以使分析数据更准确';

class list extends Component {
  constructor(props) {
    super(props);
    const { dispatch } = props;
    dispatch(initData());
    dispatch(getList({}));
  }
  render() {
    return (
      <div>
        <Filters {...this.props} />
        <TableView {...this.props} />
        <span className={Style.text}>
          <i className={Style.textName}>建议：</i>
          { suggestion }
        </span>
      </div>
    );
  }
}

list.propTypes = {
  dispatch: PropTypes.func.isRequired,
};
const mapStateToProps = state => state['checkout-check/list'];
export default connect(mapStateToProps)(list);
