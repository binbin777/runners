import { message } from 'antd';
import { takeLatest, takeEvery, put } from 'redux-saga/effects';
import { selectList, selectCheckoutList, selectCheck } from 'shein-public/service';
import * as types from './types';
import { getListSuc, changeValue, searchSuc, search } from './action';

// 获取数据源
function* getList(action) {
  const data = yield selectList(action.data);
  if (data.code !== '0') {
    return message.error(`获取数据失败: ${data.msg}`);
  }
  return yield put(getListSuc(data.data));
}

// 查询
function* searchSaga(action) {
  const data = yield selectCheckoutList(action.filter);
  if (!data || data.code !== '0') {
    message.error(`获取数据失败: ${data.msg}`);
    return yield put(changeValue('load', false));
  }
  return yield put(searchSuc(data.data));
}

// 结账检查
function* check(action) {
  const data = yield selectCheck();
  if (!data || data.code !== '0') {
    message.error(`结账检查失败: ${data.msg}`);
    return yield put(changeValue('loadBtn', false));
  }
  message.success('结账检查成功！');
  if (action.filter.companyId) {
    yield put(search(action.filter));
    return yield put(changeValue('loadBtn', false));
  }
  return yield put(changeValue('loadBtn', false));
}

export default function* () {
  yield takeLatest(types.search, searchSaga);
  yield takeLatest(types.check, check);
  yield takeEvery(types.getList, getList);
}
