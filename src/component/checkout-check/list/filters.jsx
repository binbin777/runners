import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'shineout';
import { Button } from 'antd';
import { changeValue, search, check } from './action';
import Style from './style.css';

const Filters = (props) => {
  const {
    dispatch, companyId, companyList, recentTime, loadBtn,
  } = props;
  return (
    <div className={Style.rowSpace}>
      <div className={Style.rowSpaceList}>
        <span className={Style.filterName}>公司主体:</span>
        <Select
          name="companyId"
          keygen="id"
          style={{ width: 220 }}
          data={companyList}
          onChange={(d) => {
                  dispatch(changeValue('companyId', d));
                  dispatch(search({ companyId: d }));
            }}
          datum={{ format: 'id' }}
          renderItem={item => item.companyName}
        />
        <Button
          className={Style.filterButton}
          loading={loadBtn}
          onClick={() => {
            dispatch(check({ companyId }));
          }}
        >
          结账检查
        </Button>
      </div>
      <div className={Style.rowSpaceList}>
        最近检查时间：{ recentTime }
      </div>
    </div>

  );
};
Filters.propTypes = {
  dispatch: PropTypes.func.isRequired,
  companyId: PropTypes.number,
  companyList: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  recentTime: PropTypes.string.isRequired,
  loadBtn: PropTypes.bool.isRequired,
};
export default Filters;

