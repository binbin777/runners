import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import ThousandsPoints from '../../publicComponent/thousandsPoints';

const TableView = ({
  dataSource,
  load,
}) => (
  <Table
    rowKey="id"
    pagination={false}
    size="small"
    dataSource={dataSource}
    loading={load}
    columns={[
      {
        title: '品牌',
        dataIndex: 'siteVal',
        render: text => text || '--',
      },
      {
        title: '国家',
        dataIndex: 'countryVal',
        render: text => text || '--',
      },
      {
        title: '财务期间',
        dataIndex: 'financialPeriod',
        render: text => text || '--',
      },
      {
        title: '营销费$',
        dataIndex: 'marketerFee',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '更新时间',
        dataIndex: 'updateTime',
        render: text => text || '--',
      },
    ]}
  />
);

TableView.propTypes = {
  dataSource: PropTypes.arrayOf(PropTypes.object),
  load: PropTypes.bool,
};

export default TableView;
