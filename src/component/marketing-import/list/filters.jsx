import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Form, Select, DatePicker } from 'shineout';
import moment from 'moment';
import { changeFilter, search } from './action';
import Style from './style.css';

const Filters = (props) => {
  const {
    dispatch,
    filter,
    siteList,
    params,
  } = props;
  return (
    <div className={Style.content}>
      <Form
        inline
        // rules={rules}
        onSubmit={(values) => {
          dispatch(search(Object.assign({}, values, { pageNumber: 1, pageSize: filter.pageSize })));
        }}
      >
        <Form.Item
          className={Style.filterItem}
        >
          <Select
            name="siteId"
            keygen="id"
            style={{ width: 140 }}
            data={siteList}
            onChange={d => dispatch(changeFilter('siteId', d))}
            datum={{ format: 'id' }}
            renderItem={item => item.name}
            placeholder="站点"
            clearable
            onFilter={text => item => item.name.toUpperCase().indexOf(text.toUpperCase()) >= 0}
          />
        </Form.Item>
        <Form.Item
          className={Style.filterItem}
        >
          <DatePicker
            clearable={false}
            name="financialPeriod"
            type="month"
            // defaultValue={filter.financialPeriod}
            // defaultValue={moment().subtract(1, 'months').format('YYYY-MM')}
            defaultValue={moment(params.financialPeriod ? params.financialPeriod : '').format('YYYY-MM')}
            style={{ width: 140 }}
            onChange={d => dispatch(changeFilter('financialPeriod', d))}
          />
        </Form.Item>
        <Form.Item
          className={Style.filterButton}
        >
          <Form.Submit>查询</Form.Submit>
        </Form.Item>
        <Form.Item
          className={Style.filterButton}
        >
          <Link
            className={Style.import}
            href={`/marketing-fee-import/list/${filter.financialPeriod}/${filter.processPreId}`}
            to={`/marketing-fee-import/list/${filter.financialPeriod}/${filter.processPreId}`}
            target="_blank"
            // href="/"
            // to={{
            // pathname: '/marketing-fee-import/list',
            // search: `?financialPeriod=${filter.financialPeriod}`,
            // }}
          >数据导入
          </Link>
        </Form.Item>
      </Form>
    </div>
  );
};
Filters.propTypes = {
  dispatch: PropTypes.func.isRequired,
  filter: PropTypes.shape({}).isRequired,
  siteList: PropTypes.arrayOf(PropTypes.object),
  params: PropTypes.shape().isRequired,
};
export default Filters;
