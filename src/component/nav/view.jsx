import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { BackTop, Button, Icon, Breadcrumb } from 'antd';
import { changeValue, initMainData, init } from './actions';
import Cancellation from './components/cancellation';
import { links } from './menus';
import Sider from './components/sider';
import styles from './style.css';
import Welcome from './components/welcome';

class MainPage extends React.PureComponent {
  constructor(props) {
    super(props);
    const { dispatch } = props;
    if (process.env.NODE_ENV === 'development') {
      localStorage.setItem('token', 'b728edc098d7485789ee1e70e760b406802053447');
    }
    if (window.location.search.indexOf('token') > -1) {
      dispatch(initMainData());
    } else if (localStorage.getItem('token')) {
      dispatch(init());
    } else {
      window.location.href = process.env.LOGIN_ADDR;
    }
  }
  render() {
    const {
      menus, dispatch, children, expandable, linkList, loading, userName, isCancle,
      location: {
        pathname: current,
      },
    } = this.props;

    const ma = linkList.filter(({ menuUrl: link }) => (link && (link === '/' || `${current}/`.startsWith(`${link}`)))).filter(r => r.menuUrl);
    const maSort = ma.sort((item1, item2) => item1.menuUrl.length - item2.menuUrl.length);
    let routerMatchList = maSort.filter(item => item.menuUrl);
    const flagObj = links.find(val => current.indexOf(val.reg) > -1);
    if (flagObj) {
      routerMatchList = flagObj.list;
    }
    if (!loading) return null;
    return (
      <div className={styles.antLayoutAside}>
        {/* 【财务系统logo】 */}
        <aside
          className={styles.antLayoutSider}
          style={expandable === true ? { width: '230px', zIndex: '100' } : { width: 0, zIndex: '-100' }}
        >
          <div className={styles.antLayoutLogo}>
            <Link href="/" to="/">
              财务分析系统
            </Link>
          </div>
          <Sider
            current={current}
            menus={menus}
            {...this.props}
          />
        </aside>
        {/* 【主页标题栏】，包括【伸缩按钮】、【面包屑】、【登录注销】 */}
        <div
          className={styles.antLayoutMain}
          style={expandable === true ? { marginLeft: '230px' } : { marginLeft: 0 }}
        >
          <div className={styles.antLayouttop}>
            {/* 【伸缩按钮】 */}
            <Button
              type="ghost"
              shape="circle-outline"
              className={styles.expandBtn}
              onClick={() => dispatch(changeValue('expandable', !expandable))}
            >
              <Icon
                type="swap"
                style={{ fontSize: '15px' }}
              />
            </Button>
            {/* 【面包屑】 */}
            <Breadcrumb className={styles.top_margin} separator={<span style={{ color: 'rgba(0,0,0,.65)' }}> / </span>}>
              {
                routerMatchList
                  .map(({ menuUrl, resourceName }, i) => {
                    if (i === routerMatchList.length - 1) {
                      return (
                        <Breadcrumb.Item key={menuUrl} >
                          <a href={`#${menuUrl}`} className={styles.linkcolor} >{resourceName}</a>
                        </Breadcrumb.Item>
                      );
                    }
                    return (
                      <Breadcrumb.Item key={menuUrl} >
                        <Link href={menuUrl} className={styles.linkcolor} to={menuUrl}>{resourceName}</Link>
                      </Breadcrumb.Item>
                    );
                  })
              }
            </Breadcrumb>
             {/*【用户登录&注销】*/}
            <Cancellation dispatch={dispatch} isCancle={isCancle} userName={userName} />
          </div>
          {/* 【页面内容呈现区域】 */}
          <div className={styles.antLayoutContent}>
            <BackTop visibilityHeight={100} className={styles.mobileBack} />
            {children || <Welcome />}
          </div>
        </div>
      </div>
    );
  }
}


MainPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  menus: PropTypes.arrayOf(PropTypes.object).isRequired,
  userName: PropTypes.string.isRequired,
  children: PropTypes.shape(),
  expandable: PropTypes.bool,
  linkList: PropTypes.arrayOf(PropTypes.object),
};
MainPage.defaultProps = {
  children: <Welcome />,
};
const mapStateToProps = state => state.nav;
export default connect(mapStateToProps)(MainPage);
