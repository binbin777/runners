import fetch from 'shein-lib/fetch';


export default {};


const obj = {
  allInfo: '/user/getLoginInfo',
  logout: '/logout',
};

export const allInfo = () => ( // 获取全部信息
  fetch(obj.allInfo, {
    method: 'get',
  })
);

export const logout = () => ( // 获取全部信息
  fetch(obj.logout, {
    method: 'get',
  })
);
