import assign from 'object-assign';
import * as types from './types';
import menus from './menus';

const dealMenus = (arr) => {
  if (!arr.length) return [];
  return arr.reduce((pre, cur) => {
    if (!cur.nav) return [...pre];
    if (cur.children && cur.children.length) return [...pre, assign({}, cur, { children: dealMenus(cur.children) })];
    return [...pre, cur];
  }, []);
};

function flattenArray(arr) {
  return arr.reduce((acc, current) => {
    if (current.children && current.children.length > 0) {
      return [...acc, assign(current, { type: 3 }), ...flattenArray(current.children)];
    }
    return [...acc, current];
  }, []);
}

const miniLinkList = [
  {
    menuUrl: '/checkout-check/list',
    resourceName: '结账检查',
  },
  {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  },
  // {
  //   menuUrl: '/cost-statistics/list',
  //   resourceName: '销售收入&成本统计',
  // },
  // {
  //   menuUrl: '/marketing-fee-import/list',
  //   resourceName: '营销导入',
  // },
  // {
  //   menuUrl: '/marketing-import/list',
  //   resourceName: '营销导入统计',
  // },
  // {
  //   menuUrl: '/india-middleeast-mfat/list',
  //   resourceName: '国家地区营销费分摊表',
  // },
  // {
  //   menuUrl: '/emc-ali/list',
  //   resourceName: '(emc&ali)营销费分摊表',
  // },
  {
    menuUrl: '/maori-analysis/list',
    resourceName: '商品毛利分析',
  },
  {
    menuUrl: '/siteProfit-analysis/list',
    resourceName: '站点利润分析',
  },
  {
    menuUrl: '/countryProfit-analysis/list',
    resourceName: '国家利润分析',
  },
  {
    menuUrl: '/countryCostFee-analysis/list',
    resourceName: '国家成本费用分析',
  },
];
const defaultState = {
  current: '/',
  menus: [],
  isCancle: false,
  userName: '--',
  expandable: true,
  loading: false,
  list: [],
  selectedMenu: [],
  linkList: [],
};

export default (state = defaultState, action) => {
  // state.linkList = flattenArray(menus || []).concat([{ menuUrl: '/', resourceName: '首页' }]);
  // state.linkList = miniLinkList.concat([{ menuUrl: '/', resourceName: '首页' }]);
  // console.log(state.linkList);
  switch (action.type) {
    case types.CHANGE_VALUE:
      return assign({}, state, {
        [action.key]: action.value,
      });
    case types.SET_BASE_INFO:
      return assign({}, state, {
        userId: action.obj.userId,
        userName: action.obj.userName,
        companyId: action.obj.currCompanyId,
        loading: true,
        menus: action.obj.menus || [],
        linkList: flattenArray(action.obj.menus || []).concat(miniLinkList, [{ menuUrl: '/', resourceName: '首页' }]),
        titleList: flattenArray(action.obj.menus || []).concat([{ menuUrl: '/', resourceName: '首页' }]), // 做标签名随时变化
        load: false,
      });
    case 'nav_expand':
      return assign({}, state, {
        expandable: action.value,
      });
    default:
      return state;
  }
};
