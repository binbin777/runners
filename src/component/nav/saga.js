import { takeLatest, put } from 'redux-saga/effects';
import fetch from 'shein-lib/fetchLogin';
import showMessage from 'shein-lib/modal';

import * as types from './types';
import { setBaseInfo } from './actions';
import { allInfo, logout } from './server';

function* logoutWorker() {
  yield logout();
  localStorage.removeItem('token');
}


const getAuthNavSer = () => fetch(
  '/user/getLoginInfo',
  {},
  true,
);

function* initData() {
  const info = yield allInfo();
  if (info.code === '0') {
    yield put(setBaseInfo(info.data));
  } else {
    showMessage(info.msg || '后端数据出错', false);
  }
}

function* initMainData() {
  const token = window.location.search.replace(/\?token=/, '');
  localStorage.setItem('token', token);
  const info = yield getAuthNavSer();
  if (info.code === '0') {
    window.location.href = window.location.href.replace(window.location.search, '');
  } else {
    showMessage(info.msg || '后端数据出错', false);
  }
}

export default function* () {
  yield takeLatest(types.INIT, initData);
  yield takeLatest(types.initMainData, initMainData);
  yield takeLatest(types.CANCLE, logout);
  yield takeLatest(types.LOGOUT, logoutWorker);
}
