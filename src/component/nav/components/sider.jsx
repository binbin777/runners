import React from 'react';
import { Icon, Menu } from 'antd';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Style from '../style.css';

const { SubMenu } = Menu;

const Sider = ({
  menus, current,
}) => {
  const defaultSelectedKeys = [];
  function TreeMenu({
    resourceName, icon, children, menuUrl,
  }) {
    if (current.indexOf(menuUrl) === 0) {
      defaultSelectedKeys.push(menuUrl);
    }
    if (children && children.length) {
      return (
        <SubMenu
          key={menuUrl || resourceName}
          title={<div><Icon type={icon} /><span>{resourceName}</span></div>}
        >
          {children.map(prop => TreeMenu(prop))}
        </SubMenu>
      );
    }
    return (
      <Menu.Item key={resourceName} className={Style.siderStyle}>
        {
          <Link href={menuUrl} to={menuUrl} >
            <Icon type="bars" /> {resourceName}
          </Link>
        }
      </Menu.Item>
    );
  }

  TreeMenu.propTypes = {
    icon: PropTypes.string.isRequired,
    resourceName: PropTypes.string.isRequired,
    children: PropTypes.arrayOf(PropTypes.shape(TreeMenu.propTypes)).isRequired,
    menuUrl: PropTypes.string.isRequired,
  };

  return (
    <Menu
      style={{ overflowY: 'auto', width: 'auto', height: window.innerHeight - 40 }}
      mode="inline"
      theme="dark"
      inlineIndent={12}
      defaultSelectedKeys={defaultSelectedKeys}
      defaultOpenKeys={defaultSelectedKeys}
      className={Style.siderStyle}
    >
      {
        menus.map(TreeMenu)
      }
    </Menu>
  );
};

Sider.propTypes = {
  current: PropTypes.string.isRequired,
  menus: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Sider;
