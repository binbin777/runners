import React from 'react';
import PropTypes from 'prop-types';
import Icon from '@shein-components/Icon';
import { Button } from 'shineout';
import { changeValue, cancle } from '../actions';
import style from '../style.css';

const Cancellation = (props) => {
  const { isCancle, dispatch, userName } = props;
  if (isCancle) {
    return (
      <div className={style.userInfo}>
        <img alt="" src="http://sheinsz.ltwebstatic.com/she_dist/images/touch-icon-ipad-120-601ddff8b5.png" />
        <span style={{ color: '#666C7C' }}>{userName}&nbsp;&nbsp;</span>
        <Icon name="triangle-down" onClick={() => dispatch(changeValue('isCancle', false))} />
        <Button
          className={style.btn}
          onClick={() => dispatch(cancle())}
        >注销
        </Button>
      </div>
    );
  }
  return (
    <div className={style.userInfo}>
      <img alt="" src="http://sheinsz.ltwebstatic.com/she_dist/images/touch-icon-ipad-120-601ddff8b5.png" />
      <span style={{ color: '#666C7C' }}>{userName}&nbsp;&nbsp;</span>
      <Icon name="triangle-up" onClick={() => dispatch(changeValue('isCancle', true))} />
    </div>
  );
};

Cancellation.propTypes = {
  isCancle: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
  userName: PropTypes.string.isRequired,
};

export default Cancellation;
