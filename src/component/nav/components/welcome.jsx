import React from 'react';
import styles from '../style.css';

export default () => (

  <div className={styles.demo} style={{ height: window.innerHeight - 74 }}>
    <p>
      {
        ['财', '务', '分', '析'].map(v => <span key={v}>{v}</span>)
      }
    </p>
  </div>
);
