export default [
  {
    resourceName: '结账检查',
    key: 'checkout-check',
    menuUrl: '/checkout-check/list',
    nav: true,
  },
  {
    resourceName: '前置处理',
    key: 'preprocess',
    menuUrl: '/preprocess/list',
    nav: true,
    children: [
      {
        resourceName: '销售收入&成本统计', key: 'cost-statistics', menuUrl: '/cost-statistics/list', nav: false,
      },
      {
        resourceName: '营销费导入', key: 'marketing-fee-import', menuUrl: '/marketing-fee-import/list', nav: false,
      },
      {
        resourceName: '营销费导入统计', key: 'marketing-import', menuUrl: '/marketing-import/list', nav: false,
      },
      {
        resourceName: '国家地区营销费分摊表', key: 'india-middleeast-mfat', menuUrl: '/india-middleeast-mfat/list', nav: false,
      },
      {
        resourceName: '(emc&ali)营销费分摊表', key: 'emc-ali', menuUrl: '/emc-ali/list', nav: false,
      },
      {
        resourceName: '手续费统计', key: 'fee-statistics', menuUrl: '/fee-statistics/list', nav: false,
      },
      {
        resourceName: '仓储费&物流费用统计', key: 'warehous-and-logistics-fee-statistics', menuUrl: '/warehous-and-logistics-fee-statistics/list', nav: false,
      },
      {
        resourceName: '待摊运费分摊表', key: 'amortized-freight-distribution', menuUrl: '/amortized-freight-distribution/list', nav: false,
      },
      {
        resourceName: '待摊仓储费分摊表', key: 'warehouse-allocation', menuUrl: '/warehouse-allocation/list', nav: false,
      },
      {
        resourceName: '待摊税费分摊表', key: 'amortization-and-apportionment', menuUrl: '/amortization-and-apportionment/list', nav: false,
      },
      {
        resourceName: '国家地区配置', key: 'countryArea', menuUrl: '/countryArea/list', nav: false,
      },
      {
        resourceName: 'CODS服务费统计', key: 'CODS-service-statistics', menuUrl: '/CODS-service-statistics/list', nav: false,
      },
      {
        resourceName: 'CODS服务费导入', key: 'CODS-service-import', menuUrl: '/CODS-service-import/list', nav: false,
      },
      {
        resourceName: 'FBA运费统计', key: 'FBA-statistics', menuUrl: '/FBA-statistics/list', nav: false,
      },
      {
        resourceName: 'FBA运费导入', key: 'FBA-import', menuUrl: '/FBA-import/list', nav: false,
      },
      {
        resourceName: '仓储费统计', key: 'warehousing-statistics', menuUrl: '/warehousing-statistics/list', nav: false,
      },
      {
        resourceName: '仓储费导入', key: 'warehousing-statistics', menuUrl: '/warehousing-import/list', nav: false,
      },
    ],
  },
  {
    resourceName: '商品毛利分析',
    key: 'maori-analysis',
    menuUrl: '/maori-analysis/list',
    nav: true,
  },
  {
    resourceName: '站点利润分析',
    key: 'siteProfit-analysis',
    menuUrl: '/siteProfit-analysis/list',
    nav: true,
  },
  {
    resourceName: '国家利润分析',
    key: 'countryProfit-analysis',
    menuUrl: '/countryProfit-analysis/list',
    nav: true,
  },
  {
    resourceName: '国家成本费用分析',
    key: 'countryCostFee-analysis',
    menuUrl: '/countryCostFee-analysis/list',
    nav: true,
  },
];

export const links = [{
  reg: '/cost-statistics/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/cost-statistics/list',
    resourceName: '销售收入&成本统计',
  }],
},
{
  reg: '/marketing-fee-import/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/marketing-fee-import/list',
    resourceName: '营销费导入',
  }],
},
{
  reg: '/marketing-import/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/marketing-import/list',
    resourceName: '营销费导入统计',
  }],
},
{
  reg: '/india-middleeast-mfat/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/india-middleeast-mfat/list',
    resourceName: '国家地区营销费分摊表',
  }],
},
{
  reg: '/emc-ali/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/emc-ali/list',
    resourceName: '(emc&ali)营销费分摊表',
  }],
},
{
  reg: '/fee-statistics/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/fee-statistics/list',
    resourceName: '手续费统计',
  }],
},
{
  reg: '/warehous-and-logistics-fee-statistics/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/warehous-and-logistics-fee-statistics/list',
    resourceName: '仓储费&物流费用统计',
  }],
},
{
  reg: '/amortized-freight-distribution/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/amortized-freight-distribution/list',
    resourceName: '待摊运费分摊表',
  }],
},
{
  reg: '/warehouse-allocation/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/warehouse-allocation/list',
    resourceName: '待摊仓储费分摊表',
  }],
},
{
  reg: '/amortization-and-apportionment/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/amortization-and-apportionment/list',
    resourceName: '待摊税费分摊统计',
  }],
},
{
  reg: '/countryArea/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/countryArea/list',
    resourceName: '国家地区配置',
  }],
},
{
  reg: '/CODS-service-statistics/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/CODS-service-statistics/list',
    resourceName: 'CODS服务费统计',
  }],
},
{
  reg: '/CODS-service-import/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/CODS-service-import/list',
    resourceName: 'CODS服务费导入',
  }],
},
{
  reg: '/FBA-statistics/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/FBA-statistics/list',
    resourceName: 'FBA运费统计',
  }],
},
{
  reg: '/FBA-import/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/FBA-import/list',
    resourceName: 'FBA运费导入',
  }],
},
{
  reg: '/warehousing-statistics/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/warehousing-statistics/list',
    resourceName: '仓储费统计',
  }],
},
{
  reg: '/warehousing-import/list',
  list: [{ menuUrl: '/', resourceName: '首页' }, {
    menuUrl: '/preprocess/list',
    resourceName: '前置处理',
  }, {
    menuUrl: '/warehousing-import/list',
    resourceName: '仓储费导入',
  }],
},
];
