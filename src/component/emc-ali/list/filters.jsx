import React from 'react';
import PropTypes from 'prop-types';
import { Form, Select, DatePicker } from 'shineout';
import moment from 'moment';
import { changeFilter, search } from './action';
import Style from './style.css';

const Filters = (props) => {
  const site = ['emmacloth', 'Aliexpress'];
  const {
    dispatch,
    siteList,
    params,
  } = props;
  return (
    <div className={Style.content}>
      <Form
        inline
        onSubmit={(values) => {
          dispatch(search(values));
        }}
      >
        <Form.Item
          className={Style.filterItem}
        >
          <Select
            name="siteId"
            keygen="id"
            style={{ width: 140 }}
            data={siteList.filter(val => site.indexOf(val.name) > -1)}
            onChange={d => dispatch(changeFilter('siteId', d))}
            datum={{ format: 'id' }}
            renderItem={item => item.name}
            clearable
            placeholder="站点"
            onFilter={text => item => item.name.toUpperCase().indexOf(text.toUpperCase()) >= 0}
          />
        </Form.Item>
        <Form.Item
          className={Style.filterItem}
        >
          <DatePicker
            clearable={false}
            name="financialPeriod"
            type="month"
            // defaultValue={filter.financialPeriod}
            // defaultValue={moment().subtract(1, 'months').format('YYYY-MM')}
            defaultValue={moment(params.id ? params.id : '').format('YYYY-MM')}
            style={{ width: 140 }}
            onChange={d => dispatch(changeFilter('financialPeriod', d))}
          />
        </Form.Item>
        <Form.Item
          className={Style.filterButton}
        >
          <Form.Submit>查询</Form.Submit>
        </Form.Item>
      </Form>
    </div>
  );
};
Filters.propTypes = {
  dispatch: PropTypes.func.isRequired,
  siteList: PropTypes.arrayOf(PropTypes.shape({})),
  params: PropTypes.shape().isRequired,
};
export default Filters;
