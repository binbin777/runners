/*
** 7，emc-ali,光彬
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Filters from './filters';
import TableView from './table';
import { initData, getList, changeFilter } from './action';

class list extends Component {
  constructor(props) {
    super(props);
    const { dispatch, params } = props;
    dispatch(initData());
    dispatch(getList());
    dispatch(changeFilter('financialPeriod', params.id ? params.id : ''));
  }
  render() {
    return (
      <div>
        <Filters {...this.props} />
        <TableView {...this.props} />
      </div>
    );
  }
}

list.propTypes = {
  dispatch: PropTypes.func,
  filter: PropTypes.shape({}),
  total: PropTypes.number,
  current: PropTypes.number,
  params: PropTypes.shape().isRequired,
};
const mapStateToProps = state => state['emc-ali/list'];
export default connect(mapStateToProps)(list);
