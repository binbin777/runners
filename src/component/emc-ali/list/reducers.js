import * as types from './types';


export const defaultState = {
  filter: {
    financialPeriod: '',
    siteId: null,
  },
  dataSource: [],
  load: false,
  total: 0,
  siteList: [],
};

const reducer = {
  defaultState,
  [types.initData]: () => defaultState,
  [types.changeValue]: (state, { key, value }) => { state[key] = value; },
  [types.changeFilter]: (state, { key, value }) => { state.filter[key] = value; },
  [types.search]: (state) => {
    state.load = true;
  },
  [types.searchSuc]: (state, { data }) => {
    state.dataSource = data;
    state.load = false;
  },
  [types.getListSuc]: (state, { data }) => {
    state.siteList = data.siteList;
  },
};
export default reducer;
