/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import { Button, Upload, message } from 'antd';
import Style from './style.css';
import { changeValue, confirmExport } from './action';
import downloadUrl from '../../../model/CODS服务费导入模板.xlsx';

const Header = (props) => {
  const {
    dispatch, filter, code, dataSource, load, disable,
  } = props;
  const { financialPeriod, processPreId } = filter;
  // const downloadUrl = require('../../../model/营销费导入_模版.xlsx'); // 下载：营销导入样表 地址

  const setProps = { // 上传按钮配置参数
    name: 'file',
    accept: '.xlsx',
    action: `${process.env.BASE_URI}/codsServiceFee/uploadFile`,
    headers: {
      authorization: 'authorization-text',
      token: localStorage.getItem('token'),
    },
    showUploadList: false,
    data: {
      financialPeriod,
    },
    onChange(info) {
      if (info.file.status === 'done') {
        // console.log(info.file, 'info.file');
        dispatch(changeValue('load', true));
        dispatch(changeValue('code', info.file.response.code));
        dispatch(changeValue('dataSource', info.file.response.data || {})); // 将返回的数据赋值给dataSource
        dispatch(changeValue('load', false));
        if (info.file.response.code !== '0') {
          message.error(info.file.response.msg);
        }
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };
  return (
    <div>
      <div className={Style.rowSpace}>
        <div className={Style.rowSpaceList}>
          <b>财务期间：{financialPeriod}</b>
        </div>
        <div className={Style.rowSpaceList}>
          <a
            href={downloadUrl}
            target="_blank"
            download="CODS服务费导入模板.xlsx"
          >下载: CODS服务费导入表样
          </a>
        </div>
        <div className={Style.rowSpaceList}>
          <Upload {...setProps}>
            <Button>
              选择导入文件
            </Button>
          </Upload>
          &nbsp;
          {
            // 短路操作
            code === '0' &&
            (dataSource.errorNumber === 0 ?
              <Button loading={load} disabled={disable} onClick={() => { dispatch(confirmExport({ financialPeriod, processPreId })); }}>确认导入</Button> : '')
          }
          {
            // 短路操作
            code === '0' &&
            (dataSource.errorNumber === 0 ?
              <span style={{ margin: '0px 10px' }}>文件记录校验通过</span> : <span style={{ color: 'red', margin: '0px 10px' }}>导入文件校验不通过，请修正错误后重新导入</span>)
          }
          {
            // 上传文件无效
            code === '-1' ? <span style={{ color: 'red', margin: '0px 10px' }}>上传文件无效</span> : ''
          }
        </div>
      </div>
      <div className={Style.flexDisplay}>
        <b>文件记录</b>
        {
          code !== '-1' &&
          (code === '0' ?
            <b>共有({dataSource.totalNumber})条记录，正确记录({dataSource.correctNumber})条，错误记录(<span style={{ color: 'red' }}>{dataSource.errorNumber}</span>)条</b>
            :
            '')
        }
      </div>
    </div>
  );
};
Header.propTypes = {
  dispatch: PropTypes.func.isRequired,
  filter: PropTypes.shape({}).isRequired,
  dataSource: PropTypes.shape({}).isRequired,
  code: PropTypes.string,
  location: PropTypes.shape({}).isRequired,
  search: PropTypes.string,
  load: PropTypes.bool,
  disable: PropTypes.bool,
};
export default Header;
/* eslint-disable global-require */
