import { message } from 'antd';
import { put, takeLatest } from 'redux-saga/effects';
import { confirmCODS } from 'shein-public/service';
import * as types from './types';
import { changeValue } from './action';

// 确认导入
function* confirmExport(action) {
  const data = yield confirmCODS(action.value);
  if (!data || data.code !== '0') {
    message.error(`确认导入失败: ${data.msg}`);
    return yield put(changeValue('load', false));
  }
  message.success('确认导入成功！');
  yield put(changeValue('load', false));
  yield put(changeValue('disable', true));
  return setTimeout(() => window.location.reload(), 3000);
}

export default function* () {
  yield takeLatest(types.confirmExport, confirmExport);
}
