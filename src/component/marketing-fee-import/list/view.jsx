/*
*  前置处理 营销费导入 by jinchao
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Header from './header';
import TableView from './table';
import { initData, changeFilter } from './action';
import Style from './style.css';

const suggestion = '站点为"emc"或"ali"时，仅"小计"行需填写"财务期间"、"营销费用$"';

class list extends Component {
  // constructor(props) {
  //   super(props);
  //   const { dispatch, location, params } = props;
  //   dispatch(initData());
  //   // console.log(params, 'params')
  //  // dispatch(changeFilter('financialPeriod', params.id ? params.id : '')); // 获取【营销费导入统计】的财务期间 传值))
  //  // dispatch(changeFilter('financialPeriod', location.search ? location.search.split('=')[1] : '')); // 获取【营销费导入统计】的财务期间 传值))
  // }
  componentDidMount() {
    const { dispatch, params } = this.props;
    dispatch(initData());
    dispatch(changeFilter('financialPeriod', params.financialPeriod ? params.financialPeriod : ''));
    dispatch(changeFilter('processPreId', params.id ? Number(params.id) : ''));
  }
  render() {
    return (
      <div>
        <Header {...this.props} />
        <TableView {...this.props} />
        <span className={Style.text}><i className={Style.textName}>说明：</i>{ suggestion }</span>
      </div>
    );
  }
}

list.propTypes = {
  dispatch: PropTypes.func,
  location: PropTypes.shape({}),
  params: PropTypes.shape().isRequired,
};

const mapStateToProps = state => state['marketing-fee-import/list'];
export default connect(mapStateToProps)(list);
