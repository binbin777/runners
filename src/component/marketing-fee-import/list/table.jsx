import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';

const TableView = ({
  dataSource,
  load,
}) => (
  <Table
    rowKey="id"
    pagination={false}
    size="small"
    dataSource={(dataSource.checkList || []).map((v, i) => (Object.assign({}, v, { id: i })))}
    loading={load}
    columns={[
      {
        title: '站点',
        dataIndex: 'siteVal',
        render: (text, record) => (<span style={record.errorFields.indexOf('siteVal') > -1 ? { color: 'red' } : {}}>{text}</span> || '--'),
      },
      {
        title: '国家',
        dataIndex: 'countryVal',
        render: (text, record) => (<span style={record.errorFields.indexOf('countryVal') > -1 ? { color: 'red' } : {}}>{text}</span> || '--'),
      },
      {
        title: '财务期间',
        dataIndex: 'financialPeriod',
        render: (text, record) => (<span style={record.errorFields.indexOf('financialPeriod') > -1 ? { color: 'red' } : {}}>{text}</span> || '--'),
      },
      {
        title: '营销费$',
        dataIndex: 'marketerFee',
        align: 'right',
        render: (text, record) => (<span style={record.errorFields.indexOf('marketerFee') > -1 ? { color: 'red' } : {}}>{text}</span> || '--'),
      },
    ]}
  />
);

TableView.propTypes = {
  dataSource: PropTypes.shape({}).isRequired,
  load: PropTypes.bool,
};

export default TableView;
