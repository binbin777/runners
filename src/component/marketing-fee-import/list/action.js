import * as types from './types';

export const initData = () => ({ type: types.initData });
export const changeValue = (key, value) => ({ type: types.changeValue, key, value });
export const changeFilter = (key, value) => ({ type: types.changeFilter, key, value });
export const confirmExport = value => ({ type: types.confirmExport, value });
