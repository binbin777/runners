import * as types from './types';


export const defaultState = {
  filter: {
    financialPeriod: '',
    processPreId: '',
  },
  dataSource: {},
  load: false,
  disable: false,
  code: '',
  total: 0,
};

const reducer = {
  defaultState,
  [types.initData]: () => (defaultState),
  [types.changeValue]: (state, { key, value }) => { state[key] = value; },
  [types.changeFilter]: (state, { key, value }) => { state.filter[key] = value; },
};
export default reducer;
