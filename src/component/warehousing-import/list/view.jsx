/*
*  前置处理 仓储费导入 by 光彬
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Header from './header';
import TableView from './table';
import { initData, changeFilter } from './action';


class list extends Component {
  componentDidMount() {
    const { dispatch, params } = this.props;
    dispatch(initData());
    dispatch(changeFilter('financialPeriod', params.financialPeriod ? params.financialPeriod : ''));
    dispatch(changeFilter('processPreId', params.id ? Number(params.id) : ''));
  }
  render() {
    return (
      <div>
        <Header {...this.props} />
        <TableView {...this.props} />
      </div>
    );
  }
}

list.propTypes = {
  dispatch: PropTypes.func,
  location: PropTypes.shape({}),
  params: PropTypes.shape().isRequired,
};

const mapStateToProps = state => state['warehousing-import/list'];
export default connect(mapStateToProps)(list);
