import moment from 'moment';
import assign from 'object-assign';
import * as types from './types';
import GetUid from '../../../../web_modules/shein-public/key';


export const defaultState = {
  filter: {
    pageSize: 10,
    pageNumber: 1,
    financialPeriod: moment().subtract(1, 'months').format('YYYY-MM'),
  },
  dataSource1: [],
  dataSource2: [],
  dataSource3: [],
  dataSource4: [],
  load1: false,
  load2: false,
  load3: false,
  load4: false,
  total: 0,
  siteList: [],
  countryList: [],
  companyList: [],
  classList: [],
  analysisId: 0,
  operatingTypeVal: '',
  lastUpdateTime: '',
  operatingType: null,
  operatingUrl: '',
  processPreId: null,
  feeTypeList: [
    { tableName: '待摊运费分摊表', feeType: 0 },
    { tableName: '待摊仓储费分摊表', feeType: 1 },
    { tableName: '待摊税费分摊表', feeType: 2 },
  ],
};

const reducer = {
  defaultState,
  [types.initData]: () => defaultState,
  [types.changeValue]: (state, { key, value }) => { state[key] = value; },
  [types.changeFilter]: (state, { key, value }) => { state.filter[key] = value; },
  [types.search]: (state) => {
    state.load1 = true;
    state.load2 = true;
    state.load3 = true;
    state.load4 = true;
  },
  [types.searchSuc]: (state, { data }) => {
    state.dataSource1 = data.processPreTypeOne.map(val => Object.assign({}, val, { id: GetUid() }));
    state.dataSource2 = data.processPreTypeTwo.map(val => Object.assign({}, val, { id: GetUid() }));
    state.dataSource3 = data.processPreTypeThree.map(val => Object.assign({}, val, { id: GetUid() }));
    state.dataSource4 = data.processPreTypeFour.map(val => Object.assign({}, val, { id: GetUid() }));
    state.lastUpdateTime = data.FinancialAnalysis[0].operatingList[0].lastUpdateTime;
    state.operatingTypeVal = data.FinancialAnalysis[0].operatingList[0].operatingTypeVal;
    state.processPreId = data.FinancialAnalysis[0].operatingList[0].id;
    state.operatingUrl = data.FinancialAnalysis[0].operatingList[0].operatingUrl;
    state.operatingType = data.FinancialAnalysis[0].operatingList[0].operatingType;
    state.load1 = false;
    state.load2 = false;
    state.load3 = false;
    state.load4 = false;
  },
  [types.getRate]: (state) => {
    state.load1 = true;
  },
  [types.getRateSuc]: (state, { data }) => {
    state.dataSource1[0] = assign(state.dataSource1[0], data);
    state.load1 = false;
  },
  [types.getCalculate]: (state, { load }) => {
    state[load] = true;
  },
  [types.getCalculateSuc]: (state, {
    data, name, i, index, load,
  }) => {
    state[name][i].operatingList[index] = data;
    state[load] = false;
  },
  [types.getFresh]: (state, { load }) => {
    state[load] = true;
  },
  [types.getFreshSuc]: (state, {
    data, name, i, index, load,
  }) => {
    state[name][i].operatingList[index] = data;
    state[load] = false;
  },

  [types.getCalculate3]: (state) => {
    state.load3 = true;
  },

  [types.getCalculateSuc3]: (state, { data, d }) => {
    if (state.dataSource3[1].id === d) {
      state.dataSource3[1] = assign(state.dataSource3[1], data);
    }
    if (state.dataSource3[2].id === d) {
      state.dataSource3[2] = assign(state.dataSource3[2], data);
    }
    state.load3 = false;
  },

  [types.getCalculateSuc4]: (state, { data }) => {
    state.operatingTypeVal = data.operatingTypeVal;
    state.operatingType = data.operatingType;
    state.processPreId = data.id;
  },

};
export default reducer;
