
import * as types from './types';

export const initData = () => ({ type: types.initData });
export const changeValue = (key, value) => ({ type: types.changeValue, key, value });
export const exportData = value => ({ type: types.exportData, value });
export const search = filter => ({ type: types.search, filter });
export const searchSuc = data => ({ type: types.searchSuc, data });

export const changeFilter = (key, value) => ({ type: types.changeFilter, key, value });

export const getRate = filter => ({ type: types.getRate, filter });
export const getRateSuc = data => ({ type: types.getRateSuc, data });

export const getCalculate = (filter, d, name, i, index, load) => ({
  type: types.getCalculate, filter, d, name, i, index, load,
}); // filter 传参， d 接口地址， name 前置列表名， dataSource的index, record的index, load对应表的load
export const getCalculateSuc = (data, name, i, index, load) => ({
  type: types.getCalculateSuc, data, name, i, index, load,
});
export const getFresh = (filter, d, name, i, index, load) => ({
  type: types.getFresh, filter, d, name, i, index, load,
});
export const getFreshSuc = (data, name, i, index, load) => ({
  type: types.getFreshSuc, data, name, i, index, load,
});


export const getCalculateSuc3 = (data, d) => ({ type: types.getCalculateSuc3, data, d });
export const getCalculate3 = (filter, d) => ({ type: types.getCalculate3, filter, d });

export const getCalculateSuc4 = (data, d) => ({ type: types.getCalculateSuc4, data, d });
export const getCalculate4 = (filter, d) => ({ type: types.getCalculate4, filter, d });
