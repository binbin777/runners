import React from 'react';
import PropTypes from 'prop-types';
import { Form, DatePicker } from 'shineout';
import moment from 'moment';
import { changeFilter, search } from './action';
import Style from './style.css';

const Filters = (props) => {
  const {
    dispatch,
  } = props;
  return (
    <div className={Style.flexDisplay}>
      <Form
        inline
      >
        <Form.Item
          className={Style.filterItem}
          label="财务期间"
        >
          <DatePicker
            clearable={false}
            name="financialPeriod"
            type="month"
            // defaultValue={filter.financialPeriod}
            defaultValue={moment().subtract(1, 'months').format('YYYY-MM')}
            style={{ width: 140 }}
            onChange={(d) => {
              dispatch(changeFilter('financialPeriod', d));
              dispatch(search({ financialPeriod: d }));
            }
            }
          />
        </Form.Item>
      </Form>
      <div className={Style.rowSpace}>分析开始前，请先完成下列设置：</div>
    </div>
  );
};
Filters.propTypes = {
  dispatch: PropTypes.func.isRequired,
};
export default Filters;
