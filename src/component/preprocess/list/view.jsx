/*
** 2：前置处理列表 ，光彬
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'shineout';
import { connect } from 'react-redux';
import Filters from './filters';
import TableView from './table';
import { search, initData, getCalculate4 } from './action';
import Style from './style.css';

function check(arr) {
  return arr.every((val) => {
    const operatingList = val.operatingList || [];
    return operatingList.every((v) => {
      if (v.operatingType === 0 || v.operatingType === 1) {
        return false;
      }
      return true;
    });
  });
}

class list extends Component {
  constructor(props) {
    super(props);
    const { dispatch, filter } = props;
    dispatch(initData());
    dispatch(search(filter));
  }
  render() {
    const {
      dispatch, filter, lastUpdateTime, operatingTypeVal, operatingUrl, operatingType, processPreId, dataSource2, dataSource3, dataSource4,
    } = this.props;
    // 判定是否全部计算过
    const a = check(dataSource2);
    const b = check(dataSource3);
    const c = check(dataSource4);
    const result = a && b && c;
    return (
      <div>
        <Filters {...this.props} />
        <TableView {...this.props} />
        <div className={Style.flexDisplay} style={{ paddingBottom: 30 }}>
          <div className={Style.rowSpace}>(本期间)最后一次分析时间：{lastUpdateTime || '--'}</div>
          <div className={Style.rowSpace}>{!result ? '前置表未计算，请在计算完成后开始分析' : ''}</div>
          {operatingType === 7 ?
            <Button
              disabled
              type="secondary"
              size="large"
              className={Style.button}
            >{operatingTypeVal}
            </Button> :
            <Button
              disabled={!result}
              type={!result ? 'secondary' : 'primary'}
              size="large"
              className={Style.button}
              onClick={() => dispatch(getCalculate4({ financialPeriod: filter.financialPeriod, processPreId }, operatingUrl))}
            >{operatingTypeVal}
            </Button>}
        </div>
      </div>
    );
  }
}

list.propTypes = {
  dispatch: PropTypes.func,
  filter: PropTypes.shape({}),
  total: PropTypes.number,
  current: PropTypes.number,
  operatingTypeVal: PropTypes.string,
  lastUpdateTime: PropTypes.string,
  operatingType: PropTypes.number,
  operatingUrl: PropTypes.string,
  processPreId: PropTypes.number,
  dataSource2: PropTypes.arrayOf(PropTypes.object),
  dataSource3: PropTypes.arrayOf(PropTypes.object),
  dataSource4: PropTypes.arrayOf(PropTypes.object),
};
const mapStateToProps = state => state['preprocess/list'];
export default connect(mapStateToProps)(list);
