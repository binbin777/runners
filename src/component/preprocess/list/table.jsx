import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import { Button } from 'shineout';
import { getRate, getCalculate, getFresh } from './action';
import Style from './style.css';

const TableView = ({
  dataSource1, dataSource2, dataSource3, dataSource4,
  load1, dispatch, filter, load2, load3, load4, feeTypeList,
}) => (
  <div>
    <p className={Style.title} >前置1：汇率获取</p>
    <Table
      rowKey="id"
      pagination={false}
      // dataSource={dataSource}
      dataSource={(dataSource1 || []).map((v, i) => (Object.assign({}, v, { id: i })))}
      loading={load1}
      columns={[
      {
        title: '源币种',
        dataIndex: 'baseCurrVal',
        render: text => text || '--',
      },
      {
        title: '目的币种',
        dataIndex: 'targetCurrVal',
        render: text => text || '--',
      },
      {
        title: '汇率值',
        dataIndex: 'CurRate',
        // render: text => text.toFixed(6) || '--',
        render: text => (text ? text.toFixed(6) : '--'),
      },
      {
        title: '汇率更新时间',
        dataIndex: 'maintenanceTime',
        width: 200,
        fixed: 'right',
        render: text => text || '--',
      },
      {
        title: '获取时间',
        dataIndex: 'lastUpdateTime',
        width: 200,
        fixed: 'right',
        render: (text, record) => record.operatingList[0].lastUpdateTime || '--',
      },
      {
        title: '操作',
        dataIndex: 'operatingList',
        width: 150,
        fixed: 'right',
        render: text => <Button className={Style.btn} type="link" onClick={() => dispatch(getRate(filter))}>{text[0].operatingTypeVal}</Button>,
      },
    ]}
    />
    <p className={Style.title} >前置2：收入&成本统计</p>
    <Table
      rowKey="id"
      pagination={false}
      dataSource={dataSource2}
      loading={load2}
      columns={[
        {
          title: '前置表-表名',
          dataIndex: 'tableName',
          // width: 200,
          render: (text, record) => <Link href={`${record.tableNameUrl}/${filter.financialPeriod}`} to={`${record.tableNameUrl}/${filter.financialPeriod}`} target="_blank">{text}</Link>,
        },
        {
          title: '最后数据同步时间',
          dataIndex: 'lastUpdateTime1',
          width: 250,
          fixed: 'right',
          render: (text, record) => {
            const flag = record.operatingList[0].operatingType === 11 || record.operatingList[0].operatingType === 12 || record.operatingList[0].operatingType === 13;
            if (record.operatingList[0] && flag) {
              return record.operatingList[0].lastUpdateTime ? record.operatingList[0].lastUpdateTime : '--';
            }
            return '--';
          },
        },
        {
          title: '最后计算时间',
          dataIndex: 'lastUpdateTime2',
          width: 250,
          fixed: 'right',
          render: (text, record) => {
            if (record.operatingList.length > 1) {
            if (record.operatingList[1]) {
              return record.operatingList[1].lastUpdateTime ? record.operatingList[1].lastUpdateTime : '--';
            }
            return '--';
          }
            const flag = record.operatingList[0].operatingType === 0 || record.operatingList[0].operatingType === 1 || record.operatingList[0].operatingType === 2;
            if (flag) { return record.operatingList[0].lastUpdateTime ? record.operatingList[0].lastUpdateTime : '--'; }
            return '--';
          },
        },
        {
          title: '操作',
          dataIndex: 'operatingList',
          width: 250,
          fixed: 'right',
          render: (text, record, i) => text.map((val, index) =>
            (
              <span key={index.toString()}>
                <Button
                  className={Style.btn}
                  type="link"
                  onClick={() => {
                    if (val.operatingType === 1 || val.operatingType === 12) {
                      dispatch(getFresh({ processPreId: val.id }, val.operatingUrl, 'dataSource2', i, index, 'load2'));
                      return;
                    }
                    dispatch(getCalculate({ financialPeriod: record.financialPeriod, processPreId: val.id }, val.operatingUrl, 'dataSource2', i, index, 'load2'));
                  }}
                >
                  {val.operatingTypeVal}
                </Button>
              </span>)),
        },
      ]}
    />
    <p className={Style.title} >前置3：营销费导入&分摊</p>
    <Table
      rowKey="id"
      pagination={false}
      dataSource={dataSource3}
      loading={load3}
      columns={[
        {
          title: '前置表-表名',
          dataIndex: 'tableName',
          // width: 200,
          // render: (text, record) => <Link href={`${record.tableNameUrl}/${filter.financialPeriod}`} to={`${record.tableNameUrl}/${filter.financialPeriod}`} target="_blank">{text}</Link>,
          render: (text, record) => {
            if (record.operatingList[0].operatingType === 3 || record.operatingList[0].operatingType === 5) {
              return (
                <Link
                  href={`${record.tableNameUrl}/${filter.financialPeriod}/${record.operatingList[0].id}`}
                  to={`${record.tableNameUrl}/${filter.financialPeriod}/${record.operatingList[0].id}`} target="_blank"
                >{text}
                </Link>
                     );
            }
            return (
              <Link
                href={`${record.tableNameUrl}/${filter.financialPeriod}`}
                to={`${record.tableNameUrl}/${filter.financialPeriod}`} target="_blank"
              >{text}
              </Link>
                   );
          },
        },
        {
          title: '最后更新时间',
          dataIndex: 'lastUpdateTime',
          width: 400,
          fixed: 'right',
          render: (text, record) => {
            if (record.operatingList[0]) {
              return record.operatingList[0].lastUpdateTime ? record.operatingList[0].lastUpdateTime : '--';
            }
            return '--';
          },
        },
        {
          title: '操作',
          dataIndex: 'operatingList',
          width: 250,
          fixed: 'right',
          render: (text, record, i) => text.map((val, index) =>
            (
              <span key={index.toString()}>
                <Button
                  className={Style.btn}
                  type="link"
                  onClick={() => {
                    if (val.operatingType === 1 || val.operatingType === 12) {
                      dispatch(getFresh({ processPreId: val.id }, val.operatingUrl, 'dataSource3', i, index, 'load3'));
                      return;
                    }
                    if (val.operatingType === 3 || val.operatingType === 5) {
                      // history.push(`${val.operatingUrl}/${filter.financialPeriod}`);
                      window.open(`#${val.operatingUrl}/${filter.financialPeriod}/${val.id}`, '_blank');
                      return;
                    }
                    if (val.operatingType === -1) {
                      window.open(`#${val.operatingUrl}`, '_blank');
                      return;
                    }
                    dispatch(getCalculate({ financialPeriod: record.financialPeriod, processPreId: val.id }, val.operatingUrl, 'dataSource3', i, index, 'load3'));
                  }}
                >
                  {val.operatingTypeVal}
                </Button>
              </span>)),
        },
      ]}
    />
    <p className={Style.title} >前置4：仓储物流费用导入&分摊</p>
    <Table
      rowKey="id"
      pagination={false}
      dataSource={dataSource4}
      loading={load4}
      columns={[
        {
          title: '前置表-表名',
          dataIndex: 'tableName',
          // width: 200,
          render: (text, record) => {
            if (record.operatingList[0].operatingType === 3 || record.operatingList[0].operatingType === 5) {
              return (
                <Link
                  href={`${record.tableNameUrl}/${filter.financialPeriod}/${record.operatingList[0].id}`}
                  to={`${record.tableNameUrl}/${filter.financialPeriod}/${record.operatingList[0].id}`} target="_blank"
                >{text}
                </Link>
                     );
            }
            return (
              <Link
                href={`${record.tableNameUrl}/${filter.financialPeriod}`}
                to={`${record.tableNameUrl}/${filter.financialPeriod}`} target="_blank"
              >{text}
              </Link>
                   );
          },
        },
        {
          title: '最后更新时间',
          dataIndex: 'lastUpdateTime',
          width: 400,
          fixed: 'right',
          render: (text, record) => {
            if (record.operatingList[0]) {
              return record.operatingList[0].lastUpdateTime ? record.operatingList[0].lastUpdateTime : '--';
            }
            return '--';
          },
        },
        {
          title: '操作',
          dataIndex: 'operatingList',
          width: 250,
          fixed: 'right',
          render: (text, record, i) => text.map((val, index) =>
            (
              <span key={index.toString()}>
                <Button
                  className={Style.btn}
                  type="link"
                  onClick={() => {
                  if (val.operatingType === 1 || val.operatingType === 12) {
                    dispatch(getFresh({ processPreId: val.id }, val.operatingUrl, 'dataSource4', i, index, 'load4'));
                    return;
                  }
                  if (val.operatingType === 3 || val.operatingType === 5 || val.operatingType === -1) {
                    window.open(`#${val.operatingUrl}/${filter.financialPeriod}/${val.id}`, '_blank');
                    return;
                  }
                  dispatch(getCalculate({ financialPeriod: record.financialPeriod, processPreId: val.id, feeType: feeTypeList.find((v => v.tableName === record.tableName)).feeType }, val.operatingUrl, 'dataSource4', i, index, 'load4'));
                }}
                >
                  {val.operatingTypeVal}
                </Button>
              </span>)),
        },
      ]}
    />
  </div>

);

TableView.propTypes = {
  dataSource1: PropTypes.arrayOf(PropTypes.object),
  dataSource2: PropTypes.arrayOf(PropTypes.object),
  dataSource3: PropTypes.arrayOf(PropTypes.object),
  dataSource4: PropTypes.arrayOf(PropTypes.object),
  feeTypeList: PropTypes.arrayOf(PropTypes.object),
  load1: PropTypes.bool,
  load2: PropTypes.bool,
  load3: PropTypes.bool,
  load4: PropTypes.bool,
  dispatch: PropTypes.func,
  filter: PropTypes.shape({}),
  // history: PropTypes.objectOf(),
};

export default TableView;
