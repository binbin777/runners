import { message } from 'antd';
import { takeLatest, put } from 'redux-saga/effects';
import {
  selectTable,
  selectRate,
  selectCalculate,
  selectFresh,
} from 'shein-public/service';

import * as types from './types';
import { searchSuc, changeValue, getRateSuc, getFreshSuc, getCalculateSuc, getCalculateSuc3, getCalculateSuc4 } from './action';


function* search(action) {
  const data = yield selectTable(action.filter);
  if (!data || data.code !== '0') {
    message.error(`获取数据失败: ${data.msg}`);
    yield put(changeValue('load1', false));
    yield put(changeValue('load2', false));
    return yield put(changeValue('load3', false));
  }
  return yield put(searchSuc(data.data));
}

function* getRateSaga(action) {
  const data = yield selectRate(action.filter);
  if (data.code !== '0') {
    yield put(changeValue('load1', false));
    return message.error(`获取数据失败: ${data.msg}`);
  }
  yield put(getRateSuc(data.data));
  return message.success('获取数据成功');
}

function* getCalculateSaga(action) {
  const data = yield selectCalculate(action.filter, action.d);
  if (data.code !== '0') {
    yield put(changeValue(action.load, false));
    return message.error(`获取数据失败: ${data.msg}`);
  }
  return yield put(getCalculateSuc(data.data, action.name, action.i, action.index, action.load));
}

function* getFreshSaga(action) {
  const data = yield selectFresh(action.filter, action.d);
  if (data.code !== '0') {
    yield put(changeValue(action.load, false));
    return message.error(`获取数据失败: ${data.msg}`);
  }
  return yield put(getFreshSuc(data.data, action.name, action.i, action.index, action.load));
}

function* getCalculateSaga3(action) {
  const data = yield selectCalculate(action.filter, action.d);
  if (data.code !== '0') {
    return message.error(`获取数据失败: ${data.msg}`);
  }
  return yield put(getCalculateSuc3(data.data, action.filter.processPreId));
  // return setTimeout(() => window.location.reload(), 2000);
}

function* getCalculateSaga4(action) {
  const data = yield selectCalculate(action.filter, action.d);
  if (data.code !== '0') {
    return message.error(`获取数据失败: ${data.msg}`);
  }
  return yield put(getCalculateSuc4(data.data));
}

export default function* () {
  yield takeLatest(types.search, search);
  yield takeLatest(types.getRate, getRateSaga);
  yield takeLatest(types.getCalculate, getCalculateSaga);
  yield takeLatest(types.getFresh, getFreshSaga);
  yield takeLatest(types.getCalculate3, getCalculateSaga3);
  yield takeLatest(types.getCalculate4, getCalculateSaga4);
}
