import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import ThousandsPoints from '../../publicComponent/thousandsPoints';

const TableView = ({
  dataSource,
  load,
}) => (
  <Table
    rowKey="id"
    pagination={false}
    dataSource={dataSource}
    loading={load}
    size="small"
    scroll={{ x: 1200 }}
    columns={[
      {
        title: '国家',
        dataIndex: 'countryVal',
        render: text => text || '--',
      },
      {
        title: '财务期间',
        dataIndex: 'financialPeriod',
        render: text => text || '--',
      },
      {
        title: '项目',
        dataIndex: 'projectTypeVal',
        render: text => text || '--',
      },
      {
        title: '收入$',
        dataIndex: 'revenue',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '销售成本$',
        dataIndex: 'cost',
        align: 'right',
        // render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
        render: (text, record, index) => {
          if (dataSource[index - 1]) {
            return dataSource[index - 1].revenue === 0.0 ? '#DIV/0!' : text || '--';
          }
          return text || '--';
        },
      },
      {
        title: '营销费$',
        dataIndex: 'marketerFee',
        align: 'right',
        render: (text, record, index) => {
          if (dataSource[index - 1]) {
            return dataSource[index - 1].revenue === 0.00 ? '#DIV/0!' : text || '--';
          }
          return text || '--';
        },
      },
      {
        title: '手续费$',
        dataIndex: 'cleaningFee',
        align: 'right',
        render: (text, record, index) => {
          if (dataSource[index - 1]) {
            return dataSource[index - 1].revenue === 0.00 ? '#DIV/0!' : text || '--';
          }
          return text || '--';
        },
      },
      {
        title: '运费$',
        dataIndex: 'freight',
        align: 'right',
        render: (text, record, index) => {
          if (dataSource[index - 1]) {
            return dataSource[index - 1].revenue === 0.00 ? '#DIV/0!' : text || '--';
          }
          return text || '--';
        },
      },
      {
        title: '仓储费$',
        dataIndex: 'warehousingFee',
        align: 'right',
        render: (text, record, index) => {
          if (dataSource[index - 1]) {
            return dataSource[index - 1].revenue === 0.00 ? '#DIV/0!' : text || '--';
          }
          return text || '--';
        },
      },
      {
        title: '税费$',
        dataIndex: 'taxation',
        align: 'right',
        render: (text, record, index) => {
          if (dataSource[index - 1]) {
            return dataSource[index - 1].revenue === 0.00 ? '#DIV/0!' : text || '--';
          }
          return text || '--';
        },
      },
      {
        title: 'CODS服务费$',
        dataIndex: 'codsServiceFee',
        align: 'right',
        render: (text, record, index) => {
          if (dataSource[index - 1]) {
            return dataSource[index - 1].revenue === 0.00 ? '#DIV/0!' : text || '--';
          }
          return text || '--';
        },
      },
      {
        title: '利润$',
        dataIndex: 'profit',
        align: 'right',
        render: (text, record, index) => {
          if (dataSource[index - 1]) {
            return dataSource[index - 1].revenue === 0.00 ? '#DIV/0!' : text || '--';
          }
          return text || '--';
        },
      },
    ]}
  />
);

TableView.propTypes = {
  dataSource: PropTypes.arrayOf(PropTypes.object),
  load: PropTypes.bool,
};

export default TableView;
