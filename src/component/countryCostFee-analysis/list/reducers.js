import moment from 'moment';
import * as types from './types';
import GetUid from '../../../../web_modules/shein-public/key';

export const defaultState = {
  filter: {
    financialPeriod: moment().subtract(1, 'months').format('YYYY-MM'),
    countryId: null,
  },
  dataSource: [],
  load: false,
  countryList: [],
};

const reducer = {
  defaultState,
  [types.initData]: () => defaultState,
  [types.changeValue]: (state, { key, value }) => { state[key] = value; },
  [types.changeFilter]: (state, { key, value }) => { state.filter[key] = value; },
  [types.search]: (state) => {
    state.load = true;
  },
  [types.searchSuc]: (state, { data }) => {
    state.dataSource = data.map(val => Object.assign({}, val, { id: GetUid() }));
    state.load = false;
  },
  [types.getListSuc]: (state, { data }) => {
    state.countryList = data.countryList;
  },
};
export default reducer;
