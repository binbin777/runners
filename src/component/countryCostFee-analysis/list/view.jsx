/*
** 20 国家成本费用,光彬
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Filters from './filters';
import TableView from './table';
import { initData, getList } from './action';

class list extends Component {
  constructor(props) {
    super(props);
    const { dispatch } = props;
    dispatch(initData());
    dispatch(getList());
  }
  render() {
    return (
      <div>
        <Filters {...this.props} />
        <TableView {...this.props} />
      </div>
    );
  }
}

list.propTypes = {
  dispatch: PropTypes.func,
};
const mapStateToProps = state => state['countryCostFee-analysis/list'];
export default connect(mapStateToProps)(list);
