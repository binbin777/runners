import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';

const TableView = ({
  dataSource,
  load,
}) => (
  <Table
    rowKey="id"
    pagination={false}
    size="small"
    dataSource={(dataSource.checkList || []).map((v, i) => (Object.assign({}, v, { id: i })))}
    loading={load}
    columns={[
      {
        title: '站点',
        dataIndex: 'siteVal',
        render: (text, record) => (<span style={record.errorFields.indexOf('siteVal') > -1 ? { color: 'red' } : {}}>{text}</span> || '--'),
      },
      {
        title: '物流服务商',
        dataIndex: 'supplierVal',
        render: (text, record) => (<span style={record.errorFields.indexOf('supplierVal') > -1 ? { color: 'red' } : {}}>{text}</span> || '--'),
      },
      {
        title: '国家',
        dataIndex: 'countryVal',
        render: (text, record) => (<span style={record.errorFields.indexOf('countryVal') > -1 ? { color: 'red' } : {}}>{text}</span> || '--'),
      },
      {
        title: '支付方式',
        dataIndex: 'orderTypeVal',
        render: (text, record) => (<span style={record.errorFields.indexOf('orderTypeVal') > -1 ? { color: 'red' } : {}}>{text}</span> || '--'),
      },
      {
        title: '财务期间',
        dataIndex: 'financialPeriod',
        render: (text, record) => (<span style={record.errorFields.indexOf('financialPeriod') > -1 ? { color: 'red' } : {}}>{text}</span> || '--'),
      },
      {
        title: '运费成本$',
        dataIndex: 'freight',
        align: 'right',
        render: (text, record) => (<span style={record.errorFields.indexOf('freight') > -1 ? { color: 'red' } : {}}>{text}</span> || '--'),
      },
      {
        title: '备注',
        dataIndex: 'remark',
        render: (text, record) => (<span style={record.errorFields.indexOf('remark') > -1 ? { color: 'red' } : {}}>{text}</span> || '--'),
      },
    ]}
  />
);

TableView.propTypes = {
  dataSource: PropTypes.shape({}).isRequired,
  load: PropTypes.bool,
};

export default TableView;
