/*
** 10：仓储费&物流费用统计,光彬
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Page from 'shein-public/pagination';
import { connect } from 'react-redux';
import Filters from './filters';
import TableView from './table';
import { search, initData, getList, changeFilter } from './action';
import style from './style.css';
import ThousandsPoints from '../../publicComponent/thousandsPoints';

class list extends Component {
  constructor(props) {
    super(props);
    const { dispatch, params } = props;
    dispatch(initData());
    dispatch(getList());
    dispatch(changeFilter('financialPeriod', params.id ? params.id : ''));
  }
  render() {
    const {
      dispatch, filter, total, summary, curRate, maintenanceTime,
    } = this.props;
    return (
      <div>
        <Filters {...this.props} />
        <div className={style.summary}>
          <span>汇总:运费成本-发货$:{typeof (summary.freightUsdSum) === 'number' ? ThousandsPoints(summary.freightUsdSum.toFixed(2)) : ''}</span>
          <span>运费成本-退货$:{typeof (summary.returnFreightUsdSum) === 'number' ? ThousandsPoints(summary.returnFreightUsdSum.toFixed(2)) : ''}</span>
          <span>仓储费$:{typeof (summary.warehousingFeeUsdSum) === 'number' ? ThousandsPoints(summary.warehousingFeeUsdSum.toFixed(2)) : ''}</span>
          <span>税费$:{typeof (summary.taxationUsdSum) === 'number' ? ThousandsPoints(summary.taxationUsdSum.toFixed(2)) : ''}</span>
        </div>
        <TableView {...this.props} />
        <span style={{ display: 'inline-block', margin: '25px 10px' }}>(1.000000)￥ &rsaquo;&rsaquo; ({ typeof (curRate) === 'number' ? curRate.toFixed(6) : ' '})$，更新时间：{maintenanceTime}</span>
        <Page
          total={total}
          current={filter.pageNumber}
          onChange={(page, size) => (
            dispatch(search(Object.assign({}, filter, { pageNumber: page, pageSize: size })))
          )}
        />
      </div>
    );
  }
}

list.propTypes = {
  dispatch: PropTypes.func,
  filter: PropTypes.shape({}),
  total: PropTypes.number,
  current: PropTypes.number,
  summary: PropTypes.shape({}),
  params: PropTypes.shape().isRequired,
  maintenanceTime: PropTypes.string,
  curRate: PropTypes.number,
};
const mapStateToProps = state => state['warehous-and-logistics-fee-statistics/list'];
export default connect(mapStateToProps)(list);
