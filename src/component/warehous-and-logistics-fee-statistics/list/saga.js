
import { message } from 'antd';
import { takeLatest, takeEvery, put } from 'redux-saga/effects';
import {
  selectList,
  selectWarehouseLogisticsSummary,
} from 'shein-public/service';

import * as types from './types';
import { searchSuc, changeValue, getListSuc } from './action';

function* search(action) {
  const data = yield selectWarehouseLogisticsSummary(action.filter);
  if (!data || data.code !== '0') {
    message.error(`获取数据失败: ${data.msg}`);
    return yield put(changeValue('load', false));
  }
  return yield put(searchSuc(data.data));
}

function* getList() {
  const data = yield selectList();
  if (data.code !== '0') {
    return message.error(`获取数据失败: ${data.msg}`);
  }
  return yield put(getListSuc(data.data));
}

export default function* () {
  yield takeLatest(types.search, search);
  yield takeEvery(types.getList, getList);
}
