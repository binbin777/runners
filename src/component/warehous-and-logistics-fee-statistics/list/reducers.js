import * as types from './types';


export const defaultState = {
  filter: {
    pageSize: 20,
    pageNumber: 1,
    financialPeriod: '',
    siteId: '',
    companyId: '',
    orderType: '',
    countryId: null,
    projectType: '',
    supplierId: '',
  },
  dataSource: [],
  load: false,
  maintenanceTime: '',
  curRate: null,
  total: 0,
  siteList: [
    {
      id: 0,
      name: '无站点',
    },
  ],
  companyList: [],
  orderTypeList: [
    { id: 0, name: '在线' },
    { id: 1, name: 'COD' },
  ],
  projectTypeList: [
    { id: 1, name: '记暂估' },
    { id: 2, name: '冲暂估' },
    { id: 3, name: '记应付' },
  ],
  supplierList: [],
  summary: {},
  countryList: [
    {
      id: 0,
      code: '',
      nameEn: 'none country',
      nameCn: '无国家',
    },
  ],
};

const reducer = {
  defaultState,
  [types.initData]: () => defaultState,
  [types.changeValue]: (state, { key, value }) => { state[key] = value; },
  [types.changeFilter]: (state, { key, value }) => { state.filter[key] = value; },
  [types.search]: (state, { filter }) => {
    state.load = true;
    state.filter.pageSize = filter.pageSize;
    state.filter.pageNumber = filter.pageNumber;
  },
  [types.searchSuc]: (state, { data }) => {
    state.dataSource = data.rows;
    state.total = data.total;
    state.summary = data.summary;
    state.maintenanceTime = data.maintenanceTime;
    state.curRate = data.curRate;
    state.load = false;
  },
  [types.getListSuc]: (state, { data }) => {
    state.siteList = state.siteList.concat(data.siteList);
    state.companyList = data.companyList;
    state.countryList = state.countryList.concat(data.countryList);
    state.supplierList = data.supplierList;
  },
};
export default reducer;
