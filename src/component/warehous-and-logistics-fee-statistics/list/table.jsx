import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import ThousandsPoints from '../../publicComponent/thousandsPoints';

const TableView = ({
  dataSource,
  load,
}) => (
  <Table
    rowKey="id"
    pagination={false}
    dataSource={dataSource}
    loading={load}
    size="small"
    scroll={{ x: 1700 }}
    columns={[
      {
        title: '公司',
        dataIndex: 'companyVal',
        render: text => text || '--',
      },
      {
        title: '站点',
        dataIndex: 'siteVal',
        render: text => text || '--',
      },
      {
        title: '国家',
        dataIndex: 'countryVal',
        render: text => text || '--',
      },
      {
        title: '物流服务商',
        dataIndex: 'supplierVal',
        render: text => text || '--',
      },
      {
        title: '支付方式',
        dataIndex: 'orderTypeVal',
        render: text => text || '--',
      },
      {
        title: '财务期间',
        dataIndex: 'financialPeriod',
        render: text => text || '--',
      },
      {
        title: '项目',
        dataIndex: 'projectTypeVal',
        render: text => text || '--',
      },
      {
        title: '运费-发货￥',
        dataIndex: 'freightCny',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '运费-发货$',
        dataIndex: 'freightUsd',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '运费-退货￥',
        dataIndex: 'returnFreightCny',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '运费-退货$',
        dataIndex: 'returnFreightUsd',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '仓储费￥',
        dataIndex: 'warehousingFeeCny',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '仓储费$',
        dataIndex: 'warehousingFeeUsd',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '税费￥',
        dataIndex: 'taxationCny',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '税费$',
        dataIndex: 'taxationUsd',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
    ]}
  />
);

TableView.propTypes = {
  dataSource: PropTypes.arrayOf(PropTypes.object),
  load: PropTypes.bool,
};

export default TableView;
