import * as types from './types';


export const defaultState = {
  filter: {
    pageSize: 20,
    pageNumber: 1,
    financialPeriod: '',
    siteId: '',
    companyId: '',
    lastCategoryId: '',
    countryId: null,
  },
  dataSource: [],
  load: false,
  summary: {},
  total: 0,
  siteList: [],
  companyList: [],
  classList: [],
  maintenanceTime: '',
  curRate: null,
  countryList: [],
};

const reducer = {
  defaultState,
  [types.initData]: () => defaultState,
  [types.changeValue]: (state, { key, value }) => { state[key] = value; },
  [types.changeFilter]: (state, { key, value }) => { state.filter[key] = value; },
  [types.search]: (state, { filter }) => {
    state.load = true;
    state.filter.pageSize = filter.pageSize;
    state.filter.pageNumber = filter.pageNumber;
  },
  [types.searchSuc]: (state, { data }) => {
    state.dataSource = data.rows;
    state.total = data.total;
    state.summary = data.summary;
    state.maintenanceTime = data.maintenanceTime;
    state.curRate = data.curRate;
    state.load = false;
  },
  [types.getListSuc]: (state, { data }) => {
    state.siteList = data.siteList;
    state.companyList = data.companyList;
    state.classList = data.lastCategoryList;
    state.countryList = data.countryList;
  },
};
export default reducer;
