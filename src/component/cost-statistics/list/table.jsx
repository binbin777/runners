import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import ThousandsPoints from '../../publicComponent/thousandsPoints';

const TableView = ({
  dataSource,
  load,
}) => (
  <Table
    rowKey="id"
    pagination={false}
    dataSource={dataSource}
    loading={load}
    size="small"
    scroll={{ x: 1700 }}
    columns={[
      {
        title: '公司',
        dataIndex: 'companyVal',
        render: text => text || '--',
      },
      {
        title: '站点',
        dataIndex: 'siteVal',
        render: text => text || '--',
      },
      {
        title: '国家',
        dataIndex: 'countryVal',
        render: text => text || '--',
      },
      {
        title: '财务期间',
        dataIndex: 'financialPeriod',
        render: text => text || '--',
      },
      {
        title: '末级分类名',
        dataIndex: 'lastCategoryName',
        render: text => text || '--',
      },
      {
        title: '末级分类ID',
        dataIndex: 'lastCategoryId',
        render: text => text || '--',
      },
      {
        title: '在线-确认收入$',
        dataIndex: 'recognizeRevenueOnline',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: 'COD-确认收入$',
        align: 'right',
        dataIndex: 'codRecognizeRevenue',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '在线-冲销收入$',
        align: 'right',
        dataIndex: 'reversalIncomeOnline',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: 'COD-冲销收入$',
        align: 'right',
        dataIndex: 'codReversalIncome',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '销售成本￥',
        align: 'right',
        dataIndex: 'salesCostCny',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '销售成本$',
        align: 'right',
        dataIndex: 'salesCostUsd',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '销售成本-冲销￥',
        align: 'right',
        dataIndex: 'salesCostReversalCny',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '销售成本-冲销$',
        align: 'right',
        dataIndex: 'salesCostReversalUsd',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
    ]}
  />
);

TableView.propTypes = {
  dataSource: PropTypes.arrayOf(PropTypes.object),
  load: PropTypes.bool,
};

export default TableView;
