/*
** 3：销售收入成本统计,光彬
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Page from 'shein-public/pagination';
import { connect } from 'react-redux';
import Filters from './filters';
import TableView from './table';
import { search, initData, getList, changeFilter } from './action';
import style from './style.css';
import ThousandsPoints from '../../publicComponent/thousandsPoints';

class list extends Component {
  constructor(props) {
    super(props);
    const { dispatch, params } = props;
    dispatch(initData());
    dispatch(getList());
    dispatch(changeFilter('financialPeriod', params.id ? params.id : ''));
  }
  render() {
    const {
      dispatch, filter, total, maintenanceTime, curRate, summary,
    } = this.props;
    return (
      <div>
        <Filters {...this.props} />
        <div className={style.summary}>
          汇总:
          <span>在线-确认收入$:<div>{typeof (summary.recognizeRevenueOnlineSum) === 'number' ? ThousandsPoints(summary.recognizeRevenueOnlineSum.toFixed(2)) : ''}</div></span>
          <span>COD-确认收入$:<div>{typeof (summary.codRecognizeRevenueSum) === 'number' ? ThousandsPoints(summary.codRecognizeRevenueSum.toFixed(2)) : ''}</div></span>
          <span>在线-冲销收入$:<div>{typeof (summary.reversalIncomeOnlineSum) === 'number' ? ThousandsPoints(summary.reversalIncomeOnlineSum.toFixed(2)) : ''}</div></span>
          <span>COD-冲销收入$:<div>{typeof (summary.codReversalIncomeSum) === 'number' ? ThousandsPoints(summary.codReversalIncomeSum.toFixed(2)) : ''}</div></span>
          <span>销售成本$:<div>{typeof (summary.salesCostUsdSum) === 'number' ? ThousandsPoints(summary.salesCostUsdSum.toFixed(2)) : ''}</div></span>
          <span>销售成本-冲销$:<div>{typeof (summary.salesCostReversalUsdSum) === 'number' ? ThousandsPoints(summary.salesCostReversalUsdSum.toFixed(2)) : ''}</div></span>
        </div>
        <TableView {...this.props} />
        <span style={{ display: 'inline-block', margin: '25px 10px' }}>(1.000000)￥ &rsaquo;&rsaquo; ({ typeof (curRate) === 'number' ? curRate.toFixed(6) : ' '})$，更新时间：{maintenanceTime}</span>
        <Page
          total={total}
          current={filter.pageNumber}
          onChange={(page, size) => (
            dispatch(search(Object.assign({}, filter, { pageNumber: page, pageSize: size })))
          )}
        />
      </div>
    );
  }
}

list.propTypes = {
  dispatch: PropTypes.func,
  filter: PropTypes.shape({}),
  total: PropTypes.number,
  current: PropTypes.number,
  maintenanceTime: PropTypes.string,
  curRate: PropTypes.number,
  summary: PropTypes.shape({}),
  params: PropTypes.shape().isRequired,
};
const mapStateToProps = state => state['cost-statistics/list'];
export default connect(mapStateToProps)(list);
