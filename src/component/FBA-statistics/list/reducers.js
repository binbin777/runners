import * as types from './types';


export const defaultState = {
  filter: {
    pageSize: 20,
    pageNumber: 1,
    financialPeriod: '',
    processPreId: '',
    supplierId: '',
  },
  dataSource: [],
  load: false,
  total: 0,
  supplierList: [],
};

const reducer = {
  defaultState,
  [types.initData]: () => defaultState,
  [types.changeValue]: (state, { key, value }) => { state[key] = value; },
  [types.changeFilter]: (state, { key, value }) => { state.filter[key] = value; },
  [types.search]: (state, { filter }) => {
    state.load = true;
    state.filter.pageSize = filter.pageSize;
    state.filter.pageNumber = filter.pageNumber;
  },
  [types.searchSuc]: (state, { data }) => {
    state.dataSource = data.rows;
    state.total = data.total;
    state.load = false;
  },
  [types.getListSuc]: (state, { data }) => {
    state.supplierList = data.supplierList;
  },
};
export default reducer;
