/*
** FBA运费统计，光彬
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Page from 'shein-public/pagination';
import { connect } from 'react-redux';
import Filters from './filters';
import TableView from './table';
import { search, initData, changeFilter, getList } from './action';

class list extends Component {
  constructor(props) {
    super(props);
    const { dispatch, params } = props;
    dispatch(initData());
    dispatch(getList());
    dispatch(changeFilter('financialPeriod', params.financialPeriod ? params.financialPeriod : ''));
    dispatch(changeFilter('processPreId', params.id ? params.id : ''));
  }
  render() {
    const {
      dispatch, filter, total,
    } = this.props;
    return (
      <div>
        <Filters {...this.props} />
        <TableView {...this.props} />
        <Page
          total={total}
          current={filter.pageNumber}
          onChange={(page, size) => (
            dispatch(search(Object.assign({}, filter, { pageNumber: page, pageSize: size })))
          )}
        />
      </div>
    );
  }
}

list.propTypes = {
  dispatch: PropTypes.func,
  filter: PropTypes.shape({}),
  total: PropTypes.number,
  current: PropTypes.number,
  params: PropTypes.shape().isRequired,
};
const mapStateToProps = state => state['FBA-statistics/list'];
export default connect(mapStateToProps)(list);
