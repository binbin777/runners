import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import { Button, Popover } from 'shineout';
import ThousandsPoints from '../../publicComponent/thousandsPoints';
import Style from './style.css';
import { deleteData } from './action';

const TableView = ({
  dataSource,
  load,
  dispatch,
  filter,
}) => {
  const content = id =>
    close => (
      <div style={{ padding: 10 }}>
        <div>确认删除当前行记录？</div>
        <div className={Style.pop} >
          <Button
            size="small"
            onClick={() => {
              close();
              dispatch(deleteData(id, filter));
            }}
          >
            确定
          </Button>
          <Button
            size="small"
            onClick={() => {
              close();
            }}
          >
            取消
          </Button>
        </div>
      </div>
    );
  return (
    <Table
      rowKey="id"
      pagination={false}
      size="small"
      dataSource={dataSource}
      loading={load}
      columns={[
      {
        title: '站点',
        dataIndex: 'siteVal',
        render: text => text || '--',
      },
      {
        title: '国家',
        dataIndex: 'countryVal',
        render: text => text || '--',
      },
      {
        title: '支付方式',
        dataIndex: 'orderTypeVal',
        render: text => text || '--',
      },
      {
        title: '财务期间',
        dataIndex: 'financialPeriod',
        render: text => text || '--',
      },
      {
        title: 'CODS服务费$',
        dataIndex: 'codsServiceFee',
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '更新时间',
        dataIndex: 'updateTime',
        render: text => text || '--',
      },
      {
        title: '操作',
        dataIndex: 'id',
        render: id =>
          (
            <Popover content={content(id)} trigger="click">
              <Button
                type="link"
                className={Style.btn}
              >
                删除
              </Button>
            </Popover>
          ),
      },
    ]}
    />
  );
};

TableView.propTypes = {
  dataSource: PropTypes.arrayOf(PropTypes.object),
  load: PropTypes.bool,
  dispatch: PropTypes.func,
  filter: PropTypes.shape({}),
};

export default TableView;
