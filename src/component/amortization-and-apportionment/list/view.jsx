/*
** 待摊税费分摊,光彬
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Page from 'shein-public/pagination';
import { connect } from 'react-redux';
import Filters from './filters';
import TableView from './table';
import { search, initData, getList, changeFilter } from './action';

class list extends Component {
  constructor(props) {
    super(props);
    const { dispatch, params } = props;
    dispatch(initData());
    dispatch(getList());
    dispatch(changeFilter('financialPeriod', params.id ? params.id : ''));
  }
  render() {
    const {
      dispatch, filter, total, curRate, maintenanceTime,
    } = this.props;
    return (
      <div>
        <Filters {...this.props} />
        <TableView {...this.props} />
        <span style={{ display: 'inline-block', margin: '25px 10px' }}>(1.000000)￥ &rsaquo;&rsaquo; ({ typeof (curRate) === 'number' ? curRate.toFixed(6) : ' '})$，更新时间：{maintenanceTime}</span>
        <Page
          total={total}
          current={filter.pageNumber}
          onChange={(page, size) => (
            dispatch(search(Object.assign({}, filter, { pageNumber: page, pageSize: size })))
          )}
        />
      </div>
    );
  }
}

list.propTypes = {
  dispatch: PropTypes.func,
  filter: PropTypes.shape({}),
  total: PropTypes.number,
  current: PropTypes.number,
  params: PropTypes.shape().isRequired,
  maintenanceTime: PropTypes.string,
  curRate: PropTypes.number,
};
const mapStateToProps = state => state['amortization-and-apportionment/list'];
export default connect(mapStateToProps)(list);
