import * as types from './types';


export const defaultState = {
  filter: {
    pageSize: 20,
    pageNumber: 1,
    financialPeriod: '',
    orderType: '',
    supplierId: '',
    feeType: 2, // 需要分摊的费用类型 0 运费 1 仓储费 2 税费
  },
  dataSource: [],
  load: false,
  maintenanceTime: '',
  curRate: null,
  total: 0,
  siteList: [
    {
      id: 0,
      name: '无站点',
    },
  ],
  orderTypeList: [
    { id: 0, name: '在线' },
    { id: 1, name: 'COD' },
  ],
  supplierList: [],
};

const reducer = {
  defaultState,
  [types.initData]: () => defaultState,
  [types.changeValue]: (state, { key, value }) => { state[key] = value; },
  [types.changeFilter]: (state, { key, value }) => { state.filter[key] = value; },
  [types.search]: (state, { filter }) => {
    state.load = true;
    state.filter.pageSize = filter.pageSize;
    state.filter.pageNumber = filter.pageNumber;
  },
  [types.searchSuc]: (state, { data }) => {
    state.dataSource = data.rows;
    state.total = data.total;
    state.summary = data.summary;
    state.maintenanceTime = data.maintenanceTime;
    state.curRate = data.curRate;
    state.load = false;
  },
  [types.getListSuc]: (state, { data }) => {
    state.supplierList = data.supplierList;
  },
};
export default reducer;
