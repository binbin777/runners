import React from 'react';
import PropTypes from 'prop-types';
import { Form, Select, DatePicker } from 'shineout';
import moment from 'moment';
import { changeFilter, search } from './action';
import Style from './style.css';


const Filters = (props) => {
  const {
    dispatch,
    filter,
    orderTypeList,
    supplierList,
    params,
  } = props;
  return (
    <div className={Style.content}>
      <Form
        inline
        onSubmit={(values) => {
          dispatch(search(Object.assign({ ...filter }, values, { pageNumber: 1, pageSize: filter.pageSize })));
        }}
      >
        <Form.Item
          className={Style.filterItem}
        >
          <Select
            name="supplierId"
            keygen="id"
            style={{ width: 160 }}
            data={supplierList}
            onChange={d => dispatch(changeFilter('supplierId', d))}
            datum={{ format: 'id' }}
            renderItem={item => item.name}
            placeholder="物流服务商"
            clearable
            // onFilter={text => item => item.nameCn.indexOf(text) >= 0}
          />
        </Form.Item>
        <Form.Item
          className={Style.filterItem}
        >
          <Select
            name="orderType"
            keygen="id"
            style={{ width: 160 }}
            data={orderTypeList}
            onChange={d => dispatch(changeFilter('orderType', d))}
            datum={{ format: 'id' }}
            renderItem={item => item.name}
            clearable
            placeholder="支付方式"
          />
        </Form.Item>
        <Form.Item
          className={Style.filterItem}
        >
          <DatePicker
            clearable={false}
            name="financialPeriod"
            type="month"
            defaultValue={moment(params.id ? params.id : '').format('YYYY-MM')}
            // value={filter.financialPeriod}
            style={{ width: 160 }}
            onChange={d => dispatch(changeFilter('financialPeriod', d))}
          />
        </Form.Item>
        <Form.Item
          className={Style.filterButton}
        >
          <Form.Submit>查询</Form.Submit>
        </Form.Item>
      </Form>
    </div>
  );
};

Filters.propTypes = {
  dispatch: PropTypes.func.isRequired,
  filter: PropTypes.shape({}).isRequired,
  orderTypeList: PropTypes.arrayOf(PropTypes.shape({})),
  supplierList: PropTypes.arrayOf(PropTypes.shape({})),
  params: PropTypes.shape().isRequired,
};
export default Filters;
