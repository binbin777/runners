/*
** 3：手续费统计,光彬
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Page from 'shein-public/pagination';
import { connect } from 'react-redux';
import Filters from './filters';
import TableView from './table';
import { search, initData, getList, changeFilter } from './action';
import style from './style.css';
import ThousandsPoints from '../../publicComponent/thousandsPoints';

class list extends Component {
  constructor(props) {
    super(props);
    const { dispatch, params } = props;
    dispatch(initData());
    dispatch(getList());
    dispatch(changeFilter('financialPeriod', params.id ? params.id : ''));
  }
  render() {
    const {
      dispatch, filter, total, summary,
    } = this.props;
    return (
      <div>
        <Filters {...this.props} />
        <div className={style.summary}>
          <span>汇总:在线-手续费$:{typeof (summary.handlingFeeUsdOnlineSum) === 'number' ? ThousandsPoints(summary.handlingFeeUsdOnlineSum.toFixed(2)) : ''}</span>
          <span>COD-手续费$:{typeof (summary.handlingFeeUsdCodSum) === 'number' ? ThousandsPoints(summary.handlingFeeUsdCodSum.toFixed(2)) : ''}</span>
          <span>手续费$:{typeof (summary.handlingFeeUsdSum) === 'number' ? ThousandsPoints(summary.handlingFeeUsdSum.toFixed(2)) : ''}</span>
        </div>
        <TableView {...this.props} />
        <Page
          total={total}
          current={filter.pageNumber}
          onChange={(page, size) => (
            dispatch(search(Object.assign({}, filter, { pageNumber: page, pageSize: size })))
          )}
        />
      </div>
    );
  }
}

list.propTypes = {
  dispatch: PropTypes.func,
  filter: PropTypes.shape({}),
  total: PropTypes.number,
  current: PropTypes.number,
  summary: PropTypes.shape({}),
  params: PropTypes.shape().isRequired,
};
const mapStateToProps = state => state['fee-statistics/list'];
export default connect(mapStateToProps)(list);
