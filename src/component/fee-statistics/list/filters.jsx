import React from 'react';
import PropTypes from 'prop-types';
import { Form, Select, DatePicker } from 'shineout';
import moment from 'moment';
import { changeFilter, search } from './action';
import Style from './style.css';

const Filters = (props) => {
  const {
    dispatch,
    filter,
    siteList,
    companyList,
    orderTypeList,
    countryList,
    params,
  } = props;
  return (
    <div className={Style.content}>
      <Form
        inline
        onSubmit={(values) => {
          dispatch(search(Object.assign({}, values, { pageNumber: 1, pageSize: filter.pageSize })));
        }}
      >
        <Form.Item
          className={Style.filterItem}
        >
          <Select
            name="companyId"
            keygen="id"
            style={{ width: 160 }}
            data={companyList}
            datum={{ format: 'id' }}
            onChange={d => dispatch(changeFilter('companyId', d))}
            renderItem={item => item.companyName}
            clearable
            placeholder="公司"
          />
        </Form.Item>
        <Form.Item
          className={Style.filterItem}
        >
          <DatePicker
            clearable={false}
            name="financialPeriod"
            type="month"
            // defaultValue={moment().subtract(1, 'months').format('YYYY-MM')}
            defaultValue={moment(params.id ? params.id : '').format('YYYY-MM')}
            // value={filter.financialPeriod}
            style={{ width: 140 }}
            onChange={d => dispatch(changeFilter('financialPeriod', d))}
          />
        </Form.Item>
        <Form.Item
          className={Style.filterItem}
        >
          <Select
            name="siteId"
            keygen="id"
            style={{ width: 140 }}
            data={siteList}
            onChange={d => dispatch(changeFilter('siteId', d))}
            datum={{ format: 'id' }}
            renderItem={item => item.name}
            clearable
            placeholder="站点"
            onFilter={text => item => item.name.toUpperCase().indexOf(text.toUpperCase()) >= 0}
          />
        </Form.Item>
        <Form.Item
          className={Style.filterItem}
        >
          <Select
            name="countryId"
            keygen="id"
            style={{ width: 160 }}
            data={countryList}
            onChange={d => dispatch(changeFilter('countryId', d))}
            datum={{ format: 'id' }}
            renderItem={item => item.nameCn}
            placeholder="国家"
            clearable
            onFilter={text => item => item.nameCn.indexOf(text) >= 0}
          />
        </Form.Item>
        <Form.Item
          className={Style.filterItem}
        >
          <Select
            name="orderType"
            keygen="id"
            style={{ width: 140 }}
            data={orderTypeList}
            onChange={d => dispatch(changeFilter('orderType', d))}
            datum={{ format: 'id' }}
            renderItem={item => item.name}
            clearable
            placeholder="支付方式"
          />
        </Form.Item>
        <Form.Item
          className={Style.filterButton}
        >
          <Form.Submit>查询</Form.Submit>
        </Form.Item>
      </Form>
    </div>
  );
};
Filters.propTypes = {
  dispatch: PropTypes.func.isRequired,
  filter: PropTypes.shape({}).isRequired,
  siteList: PropTypes.arrayOf(PropTypes.shape({})),
  companyList: PropTypes.arrayOf(PropTypes.shape({})),
  orderTypeList: PropTypes.arrayOf(PropTypes.shape({})),
  countryList: PropTypes.arrayOf(PropTypes.shape({})),
  params: PropTypes.shape().isRequired,
};
export default Filters;
