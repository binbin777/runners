import * as types from './types';


export const defaultState = {
  filter: {
    pageSize: 20,
    pageNumber: 1,
    financialPeriod: '',
    siteId: '',
    companyId: '',
    orderType: '',
    countryId: null,
  },
  dataSource: [],
  load: false,
  total: 0,
  siteList: [],
  companyList: [],
  orderTypeList: [
    { id: 0, name: '在线' },
    { id: 1, name: 'COD' },
  ],
  summary: {},
  countryList: [],
};

const reducer = {
  defaultState,
  [types.initData]: () => defaultState,
  [types.changeValue]: (state, { key, value }) => { state[key] = value; },
  [types.changeFilter]: (state, { key, value }) => { state.filter[key] = value; },
  [types.search]: (state, { filter }) => {
    state.load = true;
    state.filter.pageSize = filter.pageSize;
    state.filter.pageNumber = filter.pageNumber;
  },
  [types.searchSuc]: (state, { data }) => {
    state.dataSource = data.rows;
    state.total = data.total;
    state.summary = data.summary;
    state.load = false;
  },
  [types.getListSuc]: (state, { data }) => {
    state.siteList = data.siteList;
    state.companyList = data.companyList;
    state.countryList = data.countryList;
  },
};
export default reducer;
