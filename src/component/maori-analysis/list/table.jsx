import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import ThousandsPoints from '../../publicComponent/thousandsPoints';

const TableView = ({
  dataSource,
  load,
}) => (
  <Table
    rowKey="id"
    pagination={false}
    dataSource={dataSource}
    loading={load}
    scroll={{ x: 1700 }}
    size="small"
    // bordered
    columns={[
      {
        title: '财务期间',
        dataIndex: 'financialPeriod',
        width: 90,
        render: text => text || '--',
      },
      {
        title: '类型',
        dataIndex: 'categoryTypeVal',
        width: 70,
        render: text => text || '--',
      },
      {
        title: '分类级次',
        dataIndex: 'categoryLevel',
        width: 60,
        render: text => text || '--',
      },
      {
        title: '分类名',
        dataIndex: 'categoryName',
        width: 140,
        render: text => text || '--',
      },
      {
        title: '是否末级',
        width: 60,
        dataIndex: 'lastCategoryVal',
        render: text => text || '--',
      },
      {
        title: '一级分类ID',
        dataIndex: 'categoryFirst',
        width: 80,
        render: text => text || '--',
      },
      {
        title: '二级分类ID',
        dataIndex: 'categorySecond',
        width: 80,
        render: text => text || '--',
      },
      {
        title: '三级分类ID',
        dataIndex: 'categoryThird',
        width: 80,
        render: text => text || '--',
      },
      {
        title: '四级分类ID',
        dataIndex: 'categoryFourth',
        width: 80,
        render: text => text || '--',
      },
      {
        title: '本月销售收入-发货$',
        dataIndex: 'recognizeRevenue',
        align: 'right',
        width: 120,
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '本月销售收入-冲销$',
        dataIndex: 'reversalIncome',
        align: 'right',
        width: 120,
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '本月销售收入-净额$',
        dataIndex: 'revenueNetAmount',
        align: 'right',
        width: 120,
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '本月销售成本-发货$',
        dataIndex: 'costRevenue',
        width: 120,
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '本月销售成本-冲销$',
        dataIndex: 'costReversal',
        align: 'right',
        width: 120,
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '本月销售成本-净额$',
        dataIndex: 'costNetAmount',
        align: 'right',
        width: 120,
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '毛利润$',
        dataIndex: 'grossProfit',
        width: 90,
        align: 'right',
        render: rec => (typeof (rec) === 'number' ? ThousandsPoints(rec.toFixed(2)) : '--'),
      },
      {
        title: '毛利率%',
        dataIndex: 'grossMargin',
        width: 90,
        render: text => text || '--',
      },
    ]}
  />
);

TableView.propTypes = {
  dataSource: PropTypes.arrayOf(PropTypes.object).isRequired,
  load: PropTypes.bool,
};

export default TableView;
