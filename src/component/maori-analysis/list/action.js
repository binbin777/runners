import * as types from './types';

export const init = () => ({ type: types.init });
export const changeFilter = (key, value) => ({ type: types.changeFilter, key, value });
export const changeValue = (key, value) => ({ type: types.changeValue, key, value });
export const search = filter => ({ type: types.search, filter });
export const searchSuc = data => ({ type: types.searchSuc, data });
export const exportData = filter => ({ type: types.exportData, filter });

