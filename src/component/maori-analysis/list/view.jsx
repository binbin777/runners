/*
*  毛利分析 by jinchao
*/
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Page from 'shein-public/pagination';
import { connect } from 'react-redux';
import Filters from './filters';
import TableView from './table';
import { init, search } from './action';
import Style from './style.css';

class list extends Component {
  constructor(props) {
    super(props);
    const { dispatch } = props;
    dispatch(init());
  }
  render() {
    const {
      dispatch, filter, exchangeRate, updateTime, total,
    } = this.props;
    return (
      <div>
        <Filters {...this.props} />
        <TableView {...this.props} />
        <span className={Style.text}>
          (1.000000)￥ &rsaquo;&rsaquo; ({typeof (exchangeRate) === 'number' ? exchangeRate.toFixed(6) : ''})$，更新时间：{updateTime}
        </span>
        <Page
          total={total}
          current={filter.pageNumber}
          onChange={(page, size) => (
            dispatch(search(Object.assign({}, filter, { pageNumber: page, pageSize: size })))
          )}
        />
      </div>
    );
  }
}

list.propTypes = {
  dispatch: PropTypes.func,
  filter: PropTypes.shape({}),
  total: PropTypes.number,
  exchangeRate: PropTypes.number,
  updateTime: PropTypes.string,
};

const mapStateToProps = state => state['maori-analysis/list'];
export default connect(mapStateToProps)(list);
