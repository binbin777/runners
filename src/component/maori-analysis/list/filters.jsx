import React, { Component } from 'react';
import PropTypes from 'prop-types';
import assign from 'object-assign';
import { Form, Select, Button, Datum, DatePicker } from 'shineout';
import moment from 'moment';
import { changeFilter, search, exportData } from './action';
import Style from './style.css';


class Filters extends Component {
  constructor(props) {
    super(props);
    this.datum = new Datum.Form();
    this.handleExport = this.handleExport.bind(this);
  }

  handleExport() {
    this.datum.validate().then((res) => {
      if (res === true) this.props.dispatch(exportData(this.datum.$values));
    }).catch((e) => {
      console.log(e);
    });
  }

  render() {
    const {
      dispatch, filter, typeList, lastLevelList, classLevelList,
    } = this.props;
    return (
      <div className={Style.content}>
        <Form
          inline
          datum={this.datum}
          onSubmit={values => (dispatch(search(assign({}, values, { pageNumber: 1, pageSize: filter.pageSize })))
          )}
        >
          <Form.Item>
            <DatePicker
              clearable={false}
              name="financialPeriod"
              style={{ width: 160 }}
              type="month"
              defaultValue={moment().subtract(1, 'months').format('YYYY-MM')}
              onChange={(d) => {
                dispatch(changeFilter('financialPeriod', d));
              }}
            />
          </Form.Item>
          <Form.Item>
            <Select
              name="categoryType"
              keygen="categoryType"
              style={{ width: 160 }}
              onChange={(d) => {
                dispatch(changeFilter('categoryType', d));
              }}
              datum={{ format: 'categoryType' }}
              renderItem={item => item.typeName}
              data={typeList}
              placeholder="类型"
              clearable
            />
          </Form.Item>
          <Form.Item>
            <Select
              name="lastCategory"
              keygen="lastCategory"
              style={{ width: 160 }}
              onChange={(d) => {
                dispatch(changeFilter('lastCategory', d));
              }}
              datum={{ format: 'lastCategory' }}
              renderItem={item => item.lastLevelName}
              data={lastLevelList}
              placeholder="末级"
              clearable
            />
          </Form.Item>
          <Form.Item>
            <Select
              name="categoryLevel"
              keygen="categoryLevel"
              style={{ width: 160 }}
              onChange={(d) => {
                dispatch(changeFilter('categoryLevel', d));
              }}
              datum={{ format: 'categoryLevel' }}
              renderItem={item => item.classLevelName}
              data={classLevelList}
              placeholder="分类级次"
              clearable
            />
          </Form.Item>
          <Form.Item>
            <Form.Submit>
              查询
            </Form.Submit>
            <Button
              onClick={this.handleExport}
            >
              导出
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

Filters.propTypes = {
  dispatch: PropTypes.func.isRequired,
  filter: PropTypes.shape({}).isRequired,
  typeList: PropTypes.arrayOf(PropTypes.object).isRequired,
  lastLevelList: PropTypes.arrayOf(PropTypes.object).isRequired,
  classLevelList: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Filters;
