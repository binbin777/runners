import { message } from 'antd';
import { takeLatest, put } from 'redux-saga/effects';
import ExportModal from 'shein-public/exportModal';
import { selectMaoriTable, exportData } from 'shein-public/service';
import * as types from './types';
import { changeValue, searchSuc } from './action';

// 查询
function* search(action) {
  const data = yield selectMaoriTable(action.filter);
  if (data.msg) {
    message.error(`获取数据失败: ${data.msg}`);
    return yield put(changeValue('load', false));
  }
  return yield put(searchSuc(data.data));
}

// 导出
function* exportDataSaga(action) {
  const data = yield exportData(action.filter);
  if (data.msg) {
    message.error(`导出数据失败: ${data.msg}`);
    return yield put(changeValue('load', false));
  }
  message.success(<ExportModal />);
  return yield put(changeValue('load', false));
}

export default function* () {
  yield takeLatest(types.search, search);
  yield takeLatest(types.exportData, exportDataSaga);
}
