import moment from 'moment';
import * as types from './types';

export const defaultState = {
  filter: {
    categoryType: '', // 分类类型
    lastCategory: '', // 是否末级分类
    categoryLevel: '', // 分类级次
    financialPeriod: moment().subtract(1, 'months').format('YYYY-MM'),
    pageSize: 20,
    pageNumber: 1,
  },
  load: false,
  typeList: [
    { categoryType: 1, typeName: '小计1' },
    { categoryType: 2, typeName: '小计2' },
    { categoryType: 3, typeName: '小计3' },
    { categoryType: 4, typeName: '明细' }],
  lastLevelList: [
    { lastCategory: 0, lastLevelName: '否' },
    { lastCategory: 1, lastLevelName: '是' }],
  classLevelList: [
    { categoryLevel: 1, classLevelName: '1级' },
    { categoryLevel: 2, classLevelName: '2级' },
    { categoryLevel: 3, classLevelName: '3级' },
    { categoryLevel: 4, classLevelName: '4级' }],
  dataSource: [],
  sumInfo: {},
  exchangeRate: null,
  updateTime: '--',
  total: 0,
};

const reducer = {
  defaultState,
  [types.init]: () => (defaultState),
  [types.changeValue]: (state, { key, value }) => { state[key] = value; },
  [types.changeFilter]: (state, { key, value }) => { state.filter[key] = value; },
  [types.search]: (state, { filter }) => {
    state.load = true;
    state.filter = filter;
  },
  [types.searchSuc]: (state, { data }) => {
    state.dataSource = data.rows;
    // if (data.sumInfo) { state.dataSource = state.dataSource.concat(data.sumInfo); }
    if (data.sumInfo) { state.dataSource.push(data.sumInfo); }
    state.exchangeRate = data.curRate;
    state.updateTime = data.maintenanceTime;
    state.total = data.total;
    state.load = false;
  },
};
export default reducer;
