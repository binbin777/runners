import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Select, Button, Datum, Modal } from 'shineout';
import { changeFilter, search, addCountry } from './action';
import Style from './style.css';

const rules2 = {
  siteId: [
    { required: true, message: '请选择站点' },
  ],
  countryId: [
    { required: true, message: '请选择国家' },
  ],
};

class Filters extends Component {
  constructor(props) {
    super(props);
    this.datum = new Datum.Form();
    // this.datum2 = new Datum.Form();
    this.state = {
      visible: false,
      modalKey: Date.now(),
    };
    this.show = this.show.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }
  show() {
    this.setState({
      visible: true,
    });
  }
  handleSubmit(data) {
    this.setState({
      visible: false,
      modalKey: Date.now(),
    });
    this.props.dispatch(addCountry(data, this.props.filter));
  }

  handleClose() {
    this.setState({
      visible: false,
      modalKey: Date.now(),
    });
  }

  renderFooter() {
    return (
      <div>
        <Button onClick={this.handleClose}>取消</Button>
        <Modal.Submit>确认</Modal.Submit>
      </div>
    );
  }
  render() {
    const {
      dispatch,
      countryList,
      siteList,
      siteList2,
    } = this.props;
    return (
      <div className={Style.content}>
        <Form
          inline
          datum={this.datum}
          onSubmit={(values) => {
            dispatch(search(Object.assign({}, values)));
          }}
        >
          <Form.Item
            className={Style.filterItem}
          >
            <Select
              name="siteId"
              keygen="id"
              style={{ width: 140 }}
              data={siteList}
              onChange={d => dispatch(changeFilter('siteId', d))}
              datum={{ format: 'id' }}
              renderItem={item => item.name}
              clearable
              placeholder="站点"
              onFilter={text => item => item.name.toUpperCase().indexOf(text.toUpperCase()) >= 0}
            />
          </Form.Item>
          <Form.Item
            className={Style.filterItem}
          >
            <Select
              name="countryId"
              keygen="id"
              style={{ width: 160 }}
              data={countryList}
              onChange={d => dispatch(changeFilter('countryId', d))}
              datum={{ format: 'id' }}
              renderItem={item => item.nameCn}
              placeholder="国家"
              clearable
              onFilter={text => item => item.nameCn.indexOf(text) >= 0}
            />
          </Form.Item>
          <Form.Item
            className={Style.filterButton}
          >
            <Form.Submit>查询</Form.Submit>
            <Button
              onClick={this.show}
            >
              添加
            </Button>
            <Modal
              visible={this.state.visible}
              width={456}
              title="国家地区配置-添加"
              onClose={this.handleClose}
              footer={this.renderFooter()}
              maskCloseAble={false}
            >
              <Form
                labelWidth={100}
                rules={rules2}
                key={this.state.modalKey}
                // datum={this.datum2}
                labelAlign="right"
                style={{ maxWidth: 400 }}
                onSubmit={this.handleSubmit}
              >
                <Form.Item required label="站点">
                  <Select
                    name="siteId"
                    keygen="id"
                    // style={{ width: 140 }}
                    data={siteList2}
                    datum={{ format: 'id' }}
                    renderItem={item => item.name}
                    clearable
                  />
                </Form.Item>
                <Form.Item required label="国家">
                  <Select
                    name="countryId"
                    keygen="id"
                    // style={{ width: 140 }}
                    data={countryList}
                    datum={{ format: 'id' }}
                    renderItem={item => item.nameCn}
                    clearable
                    onFilter={text => item => item.nameCn.indexOf(text) >= 0}
                  />
                </Form.Item>
              </Form>
            </Modal>
          </Form.Item>
        </Form>
      </div>
    );
  }
}
Filters.propTypes = {
  dispatch: PropTypes.func.isRequired,
  countryList: PropTypes.arrayOf(PropTypes.shape({})),
  siteList: PropTypes.arrayOf(PropTypes.shape({})),
  siteList2: PropTypes.arrayOf(PropTypes.shape({})),
  filter: PropTypes.shape({}),
};
export default Filters;
