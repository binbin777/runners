import * as types from './types';

export const defaultState = {
  filter: {
    siteId: '',
    countryId: null,
  },
  dataSource: [],
  load: false,
  countryList: [],
  siteList: [],
  siteList2: [],
};

const reducer = {
  defaultState,
  [types.initData]: () => defaultState,
  [types.changeValue]: (state, { key, value }) => { state[key] = value; },
  [types.changeFilter]: (state, { key, value }) => { state.filter[key] = value; },
  [types.search]: (state) => {
    state.load = true;
  },
  [types.searchSuc]: (state, { data }) => {
    state.dataSource = data;
    state.load = false;
  },
  [types.getListSuc]: (state, { data }) => {
    state.countryList = data.countryList;
    state.siteList = data.siteList.filter(val => val.name === 'shein' || val.name === 'romwe');
    state.siteList2 = data.siteList.filter(val => val.name === 'shein' || val.name === 'romwe');
  },
};
export default reducer;
