
import { message } from 'antd';
import { takeLatest, takeEvery, put } from 'redux-saga/effects';
import {
  selectList,
  selectCountryArea,
  countryDelete,
  addtCountryArea,
} from 'shein-public/service';

import * as types from './types';
import { searchSuc, changeValue, getListSuc } from './action';

function* search(action) {
  const data = yield selectCountryArea(action.filter);
  if (!data || data.code !== '0') {
    message.error(`获取数据失败: ${data.msg}`);
    return yield put(changeValue('load', false));
  }
  return yield put(searchSuc(data.data));
}

function* getList() {
  const data = yield selectList();
  if (data.code !== '0') {
    return message.error(`获取数据失败: ${data.msg}`);
  }
  return yield put(getListSuc(data.data));
}

function* deleteCountry(action) {
  const data = yield countryDelete(action.id);
  if (!data || data.code !== '0') {
    return message.error(`数据删除失败: ${data.msg}`);
  }
  message.success('数据删除成功');
  yield put(changeValue('load', true));
  return yield search(action);
}

function* addCountry(action) {
  const data = yield addtCountryArea(action.data);
  if (!data || data.code !== '0') {
    return message.error(`新增数据失败: ${data.msg}`);
  }
  message.success('新增数据成功');
  yield put(changeValue('load', true));
  return yield search(action);
}

export default function* () {
  yield takeLatest(types.search, search);
  yield takeLatest(types.deleteCountry, deleteCountry);
  yield takeLatest(types.addCountry, addCountry);
  yield takeEvery(types.getList, getList);
}
