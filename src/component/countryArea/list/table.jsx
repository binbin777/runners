import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import { Button, Popover } from 'shineout';
import Style from './style.css';
import { deleteCountry } from './action';

const TableView = ({
  dataSource,
  load,
  dispatch,
  filter,
}) => {
  const content = id =>
    close => (
      <div style={{ padding: 10 }}>
        <div>确认删除当前行记录？</div>
        <div className={Style.pop} >
          <Button
            size="small"
            onClick={() => {
              close();
              dispatch(deleteCountry(id, filter));
            }}
          >
          确定
          </Button>
          <Button
            size="small"
            onClick={() => {
            close();
          }}
          >
          取消
          </Button>
        </div>
      </div>
    );

  return (
    <Table
      rowKey="id"
      pagination={false}
      dataSource={dataSource}
      loading={load}
      size="small"
      columns={[
      {
        title: '站点',
        dataIndex: 'siteVal',
        render: text => text || '--',
      },
      {
        title: '国家',
        dataIndex: 'countryVal',
        render: text => text || '--',
      },
      {
        title: '添加时间',
        dataIndex: 'addTime',
        render: text => text || '--',
      },
      {
        title: '操作',
        dataIndex: 'id',
        render: id =>
          (
            <Popover content={content(id)} trigger="click">
              <Button
                type="link"
                className={Style.btn}
              >
              删除
              </Button>
            </Popover>
          ),
      },
    ]}
    />
  );
};

TableView.propTypes = {
  dataSource: PropTypes.arrayOf(PropTypes.object),
  load: PropTypes.bool,
  dispatch: PropTypes.func,
  filter: PropTypes.shape({}),
};

export default TableView;
