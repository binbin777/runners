
import * as types from './types';

export const initData = () => ({ type: types.initData });
export const changeValue = (key, value) => ({ type: types.changeValue, key, value });
export const search = filter => ({ type: types.search, filter });
export const searchSuc = data => ({ type: types.searchSuc, data });
export const changeFilter = (key, value) => ({ type: types.changeFilter, key, value });
export const getList = () => ({ type: types.getList });
export const getListSuc = data => ({ type: types.getListSuc, data });
export const exportData = filter => ({ type: types.exportData, filter });
export const deleteCountry = (id, filter) => ({ type: types.deleteCountry, id, filter });
export const addCountry = (data, filter) => ({ type: types.addCountry, data, filter });
