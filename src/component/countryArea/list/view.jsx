/*
**  国家地区配置,光彬
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Filters from './filters';
import TableView from './table';
import { initData, getList, search } from './action';

class list extends Component {
  constructor(props) {
    super(props);
    const { dispatch, filter } = props;
    dispatch(initData());
    dispatch(getList());
    dispatch(search(filter));
  }
  render() {
    return (
      <div>
        <Filters {...this.props} />
        <TableView {...this.props} />
      </div>
    );
  }
}
list.propTypes = {
  dispatch: PropTypes.func,
  filter: PropTypes.shape({}).isRequired,
};
const mapStateToProps = state => state['countryArea/list'];
export default connect(mapStateToProps)(list);
