import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Form, DatePicker, Select } from 'shineout';
import moment from 'moment';
import { changeFilter, search } from './action';
import Style from './style.css';

const Filters = (props) => {
  const {
    dispatch,
    filter,
    params,
    countryList,
    siteList,
  } = props;
  return (
    <div className={Style.content}>
      <Form
        inline
        // rules={rules}
        onSubmit={(values) => {
          dispatch(search(Object.assign({}, values, { pageNumber: 1, pageSize: filter.pageSize })));
        }}
      >
        <Form.Item
          className={Style.filterItem}
        >
          <Select
            name="countryId"
            keygen="id"
            style={{ width: 160 }}
            data={countryList}
            onChange={d => dispatch(changeFilter('countryId', d))}
            datum={{ format: 'id' }}
            renderItem={item => item.nameCn}
            placeholder="国家"
            clearable
            onFilter={text => item => item.nameCn.indexOf(text) >= 0}
          />
        </Form.Item>
        <Form.Item
          className={Style.filterItem}
        >
          <Select
            name="siteId"
            keygen="id"
            style={{ width: 160 }}
            data={siteList}
            onChange={d => dispatch(changeFilter('siteId', d))}
            datum={{ format: 'id' }}
            renderItem={item => item.name}
            clearable
            placeholder="站点"
            onFilter={text => item => item.name.toUpperCase().indexOf(text.toUpperCase()) >= 0}
          />
        </Form.Item>
        <Form.Item
          className={Style.filterItem}
        >
          <DatePicker
            clearable={false}
            name="financialPeriod"
            type="month"
            defaultValue={moment(params.financialPeriod ? params.financialPeriod : '').format('YYYY-MM')}
            style={{ width: 160 }}
            onChange={d => dispatch(changeFilter('financialPeriod', d))}
          />
        </Form.Item>
        <Form.Item
          className={Style.filterButton}
        >
          <Form.Submit>查询</Form.Submit>
        </Form.Item>
        <Form.Item
          className={Style.filterButton}
        >
          <Link
            className={Style.import}
            href={`/warehousing-import/list/${filter.financialPeriod}/${filter.processPreId}`}
            to={`/warehousing-import/list/${filter.financialPeriod}/${filter.processPreId}`}
            target="_blank"
          >数据导入
          </Link>
        </Form.Item>
      </Form>
    </div>
  );
};
Filters.propTypes = {
  dispatch: PropTypes.func.isRequired,
  filter: PropTypes.shape({}).isRequired,
  params: PropTypes.shape().isRequired,
  countryList: PropTypes.arrayOf(PropTypes.shape({})),
  siteList: PropTypes.arrayOf(PropTypes.shape({})),
};
export default Filters;
