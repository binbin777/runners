import * as types from './types';


export const defaultState = {
  filter: {
    pageSize: 20,
    pageNumber: 1,
    financialPeriod: '',
    processPreId: '',
    countryId: '',
    siteId: '',
  },
  dataSource: [],
  load: false,
  total: 0,
  countryList: [],
  siteList: [],
};

const reducer = {
  defaultState,
  [types.initData]: () => defaultState,
  [types.changeValue]: (state, { key, value }) => { state[key] = value; },
  [types.changeFilter]: (state, { key, value }) => { state.filter[key] = value; },
  [types.search]: (state, { filter }) => {
    state.load = true;
    state.filter.pageSize = filter.pageSize;
    state.filter.pageNumber = filter.pageNumber;
  },
  [types.searchSuc]: (state, { data }) => {
    state.dataSource = data.rows;
    state.total = data.total;
    state.load = false;
  },
  [types.getListSuc]: (state, { data }) => {
    state.siteList = data.siteList;
    state.countryList = data.countryList;
  },
};
export default reducer;
