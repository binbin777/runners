/* eslint-disable */
const webpack = require('webpack');
const HappyPack = require('happypack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const cpuCount = require('os').cpus().length;
const { externals, forCombineCDNScript, forSingleCDNScript } = require('./externals');

const happyPackPool = HappyPack.ThreadPool({ size: cpuCount - 1 });//cpu



const myCDN = 'statics.sheinside.cn/common';
const isDev = process.env.NODE_ENV === 'development';

const CDNElement = () => {
  const combine = forCombineCDNScript.map(([name, pa, lock]) => {
    return `${name}/${lock.version}/${ isDev ? pa[1] || pa[0] : pa[0]}`
  });
  const single = forSingleCDNScript.map(([name, pa, lock]) => `<script src="https://${myCDN}/${name}/${lock.version}/${ isDev ? pa[1] || pa[0] : pa[0]}"></script>`);
  const combineCDN = `<script src="https://${myCDN}/??${combine.join(',')}" ></script>`;
  const singleCDN = single.join('\n');
  return combineCDN + '\n' + singleCDN;
};
module.exports = {
  entry: {
    index: ['./src/entry.jsx'],
    vendor: [
      'redux',
      'react-redux',
      'react-router-dom',
      'react-router-redux',
      'redux-saga',
      'shineout',
      'object-assign',
      'classnames',
      'whatwg-fetch',
      'lodash',
    ],
  },
  resolve: {
    modules: ['node_modules', 'web_modules'],
    extensions: ['.js', '.jsx', 'css', '.json', '.xlsx','.xls'],
  },
  externals,//避免打包
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loaders: [
          'happypack/loader?id=js',
          {
            loader: 'react-redux-component-loader',
            options: {
              externals: ['nav', 'login'],
              lazy: true,
              loading: 'Loading',
              reducerName: 'reducers',
              componentDir: 'component',
              reducerDecorator: 'reducerDecorator',
            },
          },
        ],
      },
      {
        test: /\.js$/,
        include: /node_modules\/shineout/,
        loaders: ['babel-loader'],
      },
      {
        test: /\.less$/,
        include: /node_modules\/shineout/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'less-loader',
            options: {
              modifyVars: {
                'so-theme': 'antd'
              },
            }
          }
        ]
      },
      {
        test: /component\/([^/]+\/)*type[s]?.js$/,
        exclude: /node_modules/,
        loaders: ['react-redux-types-loader'],
      },
      {
        test: /\/me\.json$/,
        use: [
          'babel-loader',
          {
            loader: 'react-redux-component-loader',
            options: {
              bundle: true,
              reducerName: 'reducers',
            },
          },
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.ejs$/,
        use: ['ejs-loader'],
      },
      {
        test: /\.(png|jpg|jpeg|gif|eot|svg|ttf|woff|xls|xlsx)$/,
        use: ['file-loader?name=[hash:base64:7].[ext]'],
      },
    ],
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: ['vendor', 'manifest'],
    }),
    new HappyPack({
      id: 'js',
      threadPool: happyPackPool,
      loaders: ['babel-loader?cacheDirectory=true'],
    }),
    new HappyPack({
      id: 'styles',
      threadPool: happyPackPool,
      loaders: ['style-loader',
        'css-loader?modules&importLoaders=1&localIdentName=[path]__[local]-[hash:base64:5]',
        'postcss-loader',
      ],
    }),
    new HtmlWebpackPlugin({
      template: './index.ejs',
      filename: isDev ? './index.html' : '../index.html',
      cdnScript: CDNElement(),
      antdVersion: require('./package-lock').dependencies.antd.version,
      hash: true,
      cache: true
    }),
    new webpack.DefinePlugin({
      'process.env': {
        LOCALE: JSON.stringify('zh-CN')
      },
    }),

  ],
};
